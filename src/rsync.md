# Rsync

> Rsync synchronise les fichiers entre 2 systèmes.

Ajouter le contrôle d'accès par ACL. Il permettra de définir les droits de l'utilisateur exécutant rsync de manière plus fine et précise.

Installer le [paquet logiciel](/linux/paquet/) `acl`.


Ajouter un utilisateur nommé `rsync` qui se chargera de l'exécution du service.

```shell-session
$ useradd rsync
```

Autoriser `rsync` à s'exécuter comme un service

[Éditer](/linux/nano) /etc/default/rsync

```
RSYNC_ENABLE=true
```

Activer et démarrer le service

```shell-session
$ nano /etc/rsyncd.conf
```

```
uid = rsync
gid = rsync

[Pi]
  path = /home/pi
  comment = Sauvegarde des fichiers de l'utilisateur pi
  read only = true
```

```shell-session
$ systemctl enable --now rsync
$ systemctl status rsync
```

Ajouter les droits en lecture sur les dossiers que rsync doit lire

```shell-session
$ setfacl --recursive -m user:rsync:r-X,default:user:rsync:r-X /home/pi
```

Retirer tous les acl sur un dossier

```shell-session
$ setfacl -b /home/pi/.ssh
```

Afficher les droits et les acl d'un dossier

```shell-session
$ getfacl /home/pi
```

Lancer la synchronisation depuis le PC

```shell-session
rsync -azv --delete-after --progress --human-readable --checksum --exclude .ssh 192.168.1.18::Pi pi
```

SFTP
Sftp toto@22.22.22.33
Get -r dossier
