---
layout: layouts/page.njk
title: Surveiller votre système
---

> ***Objectif :*** Mesurer différentes valeurs de notre système. Utilisation du proceseur, occupation de la mémoire, traffic réseau ... afin d'anticiper la résolution de problèmes
{.objectif}

## Stocker les données

Les différentes mesures seront colectées à intervalle régulier et les valeurs sotckées dans une [base de données de mesures chronologiques](../bdd/influxdb/).

sur votre PC à l'aide de Docker :

Installer [InfluxDB](../bdd/influxdb)

ou

Installer [Prometheus](../bdd/prometheus)



## Collecter les données

2 approches sont possibles

**Push** Un service sur le client se connecte au serveur et envoi les données.

**Pull** Un service sur le client expose par le biais d'un serveur web les données. Le serveur se connecte sur le client et récupère les données.

L'avantage est que plusieurs serveurs peuvent se connecter à la fréquence 

## Visualiser les données

Un logiciel présentera les données sous forme de graphique.

Installer [Grafana](../bdd/grafana) sur votre PC à l'aide de Docker