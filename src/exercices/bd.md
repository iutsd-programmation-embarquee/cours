
Réaliser une application pour téléphones Android de consultation de base de données sur la bande dessinées.

### Fonctionnement
Cette application est composée d'un champ texte, d'un bouton et d'une liste d'affichage.

L’utilisateur remplit le champ texte avec le nom d'une série de bande dessinée. Dans notre cas "<strong>A</strong>" ou "<strong>B</strong>".</p>

<p>L'utilisateur clique alors sur le bouton. L'application télécharge depuis un serveur web (localhot) les résultats de la recherche et met à jour la liste. Dans notre cas les fichiers seront <strong>A.html</strong> et <strong>B.html</strong>.</p>


Lorsque l’utilisateur entre A le programme récupère la liste A <br>
Lorsque l’utilisateur entre B le programme récupère la liste B


### Exercice 1 : Préparation des données

Mettre en forme les fichiers A.txt et B.txt afin d’être facilement lu par le programme Android. Vous choisirez le meilleur format pour l'échange de données.</p>
<p>Vous appellerez ces nouveaux fichiers <strong>A.html</strong> et <strong>B.html</strong></p>

### Exercice 2 : Préparation de l'application

Créer l'activité permettant d'afficher les éléments de l'application.

### Exercice 3 : Récupération des données
Récupérer le(s) fichier(s) par le biais d’un service web.

Vous n'oublierez pas de donner les bonnes autorisations à votre application.

### Exercice 4 : Affichage des données

Afficher les données dans une liste personnalisée. Vous afficherez le titre, le scénariste, le dessinateur, le date du dépôt légal et l’image de couverture.<br>
L’image ne devra pas être embarqué dans l’application mais téléchargée sur le serveur web.

```
Série : Quetzalcoatl
Titre : Deux fleurs de Maïs
Identifiant : 4883
Scénario : Jean-Yves Mitton
Dessin : Jean-Yves Mitton
Couleurs : Sophie Balland
Dépot légal : 04/1997
Editeur : Glénat
Collection : Vécu
ISBN : 2-7234-2321-2
Planches : 46
Couverture : Quetzalcoalt01.jpg

Série : Quetzalcoatl
Titre : La montagne de sang
Identifiant : 4884
Scénario : Jean-Yves Mitton
Dessin : Jean-Yves Mitton
Couleurs : Sophie Balland
Dépot légal : 10/1997
Editeur : Glénat
Collection : Vécu
ISBN : 2-7234-2411-1
Planches :46
Couverture : Quetzalcoalt02.jpg

Série : Quetzalcoatl
Titre : Les cauchemars de Moctezuma
Identifiant : 4885
Scénario : Jean-Yves Mitton
Dessin : Jean-Yves Mitton
Couleurs : Sophie Balland
Dépot légal : 11/1998
Editeur : Glénat
Collection : Vécu
ISBN : 2-7234-2674-2
Planches :46
Couverture : Quetzalcoalt03.jpg

Série : Quetzalcoatl
Titre : Le Dieu des Caraïbes
Identifiant : 4886
Scénario : Jean-Yves Mitton
Dessin : Jean-Yves Mitton
Couleurs : Sophie Balland
Dépot légal : 10/1999
Editeur : Glénat
Collection : Vécu
ISBN : 2-7234-2942-3
Planches :46
Couverture : Quetzalcoalt04.jpg

Série : Quetzalcoatl
Titre : La putain et le conquistador
Identifiant : 25714
Scénario : Jean-Yves Mitton
Dessin : Jean-Yves Mitton
Couleurs : Jocelyne Charrance
Dépot légal : 04/2003
Editeur : Glénat
Collection : Vécu
ISBN : 2-7234-4107-5
Planches : 46
Couverture : Quetzalcoalt05.jpg

Série : Quetzalcoatl
Titre : La noche triste
Identifiant : 49298
Scénario : Jean-Yves Mitton
Dessin : Jean-Yves Mitton
Couleurs : Jocelyne Charrance
Dépot légal : 08/2005
Editeur : Glénat
Collection : Vécu
ISBN : 2-7234-4606-9
Planches : 46
Couverture : Quetzalcoalt06.jpg

Série : Quetzalcoatl
Titre : Le secret de la Malinche
Identifiant : 72025
Scénario : Jean-Yves Mitton
Dessin : Jean-Yves Mitton
Couleurs : Jocelyne Charrance
Dépot légal : 02/2008
Editeur : Glénat
Collection : Vécu
ISBN : 978-2-7234-6042-2
Planches : 46
Couverture : Quetzalcoalt07.jpg
```

```
Série : Helldorado
Titre : Santa Maladria
Identifiant : 54882
Scénario : Jean-David Morvan, Miroslav Dragan
Dessin : Ignacio Noé
Couleurs : Ignacio Noé
Dépot légal : 04/2006
Editeur : Casterman
Collection : Ligne d'horizon
ISBN : 2-203-39307-6
Planches :46
Couverture : Helldorado01.jpg

Série : Helldorado
Titre : Esperar la muerte
Identifiant : 67888
Scénario : Jean-David Morvan, Miroslav Dragan
Dessin : Ignacio Noé
Couleurs : Ignacio Noé
Dépot légal : 04/2006
Editeur : Casterman
Collection : Ligne d'horizon
ISBN : 978-2-203-39311-0
Planches :46
Couverture : Helldorado02.jpg

Série : Helldorado
Titre : Todos Enfermos!
Identifiant : 87133
Scénario : Jean-David Morvan, Miroslav Dragan
Dessin : Ignacio Noé
Couleurs : Ignacio Noé
Dépot légal : 04/2006
Editeur : Casterman
Collection : Ligne d'horizon
ISBN : 978-2-203-01593-7
Planches :46
Couverture : Helldorado03.jpg
```