Réaliser une application pour téléphones Android de consultation de base de données sur les mangas.

### Fonctionnement

Cette application est composée d'un champ texte, d'un bouton et d'une liste d'affichage.

L’utilisateur remplit le champ texte avec le nom d'un mangaka. Dans notre cas "**A**" ou "**B**".

L'utilisateur clique alors sur le bouton. L'application télécharge depuis un serveur web (localhot) les résultats de la recherche et met à jour la liste. Dans notre cas les fichiers seront **A.html** et **B.html**.

Lorsque l’utilisateur entre A le programme récupère la liste A

Lorsque l’utilisateur entre B le programme récupère la liste B

### Exercice 1 : Préparation des données

Mettre en forme les fichiers A.txt et B.txt afin d’être facilement lu par le programme Android. Vous choisirez le meilleur format pour l'échange de données.

Vous appellerez ces nouveaux fichiers **A.html** et **B.html**

### Exercice 2 : Préparation de l'application

Créer l'activité permettant d'afficher les éléments de l'application.

### Exercice 3 : Récupération des données

Récupérer le(s) fichier(s) par le biais d’un service web.
Vous n'oublierez pas de donner les bonnes autorisations à votre application.

### Exercice 4 : Affichage des données

Afficher les données dans une liste personnalisée. Vous afficherez la série, le dessinateur, le n° ISBN, le dépôt légal,  et l’image de couverture.

L’image ne devra pas être embarqué dans l’application mais téléchargée sur le serveur web.

```
Série : Akira
Scénario : Otomo, Katsuhiro
Dessin : Otomo, Katsuhiro
Lettrage : Schmit, David
Traduction : Chollet, Sylvain
Dépot légal : 04/1999
Editeur : Glénat
ISBN : 2-7234-2737-4
Planches : 368
fichier : akira.jpg

Série : Mother Sarah
Scénario : Otomo, Katsuhiro
Dessin : Nagayasu, Takumi
Dépot légal : 05/1996
Editeur : Delcourt
ISBN : 2-84055-097-0
Planches : 151
fichier : mothersarah.jpg

Série : Dômu
Scénario : Otomo, Katsuhiro
Dessin : Otomo, Katsuhiro
Dépot légal : 08/1991
Editeur : Les Humanoïdes Associés
ISBN : 2-7316-0916-8
Planches : 92
fichier : domu.jpg

Série : Steamboy
Scénario : Otomo, Katsuhiro
Dessin : Otomo, Katsuhiro
Dépot légal : 11/2009
Editeur : Glénat
ISBN : 978-2-7234-7045-2
Planches : 184
fichier : steamboy.jpg
```

```
Série : Summer Wars 
Scénario : Sadamoto, Yoshiyuki
Dessin : Sugimoto, Iqura
Dépot légal : 11/2010
Editeur : Kazé
ISBN : 978-2-84965-842-0
Planches : 120
fichier : summerwars.jpg

Série : Neon Genesis Evangelion
Scénario : Sadamoto, Yoshiyuki
Dessin : Sadamoto, Yoshiyuki
Lettrage : Bakayaro 
Dépot légal : 02/1998
Editeur : Glénat
ISBN : 2-7234-2517-7
Planches :168
fichier : evangelion.jpg

Série : Les enfants Loups 
Scénario : Hosoda, Mamoru
Dessin : Yû
Dépot légal : 06/2013
Editeur : Kazé
ISBN : 978-2-8203-0632-6
Planches :148
fichier : enfantsloups.jpg
```