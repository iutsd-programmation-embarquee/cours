---
layout: layouts/page.njk
title: Outil de comparaison 
---

Le premier outil, dans la gestion d'un ensemble de codes sources, est l'outil de comparaison de fichier. Il permet d'isoler les différences de contenu dans un ensemble de fichier. Son algorithme utilise la plus longue sous-séquence commune entre deux fichiers pour la supprimer ou l'insérer selon le besoin de la présentation.


## Questions

1. Quels sont les fichiers présents dans Cuisine A et pas dans Cuisine B ?
« View - Show Left Unique Items »

2. Quels sont les fichiers présents dans Cuisine B et pas dans Cuisine C ? 
« View - Show Right Unique Items »

3. Dans le fichier « Biscuits aux mirabelles » Quel est l’ingrédient présent à droite et pas à gauche ?
Les différences apparaissent en jaune, les lignes manquantes en gris

4. Dans le fichier « Puddings aux myrtilles » la recette est écrite sans retour à la ligne, on ne peut isoler précisément la différence (tout est jaune). Plutôt que d’écrire tout sur une seule ligne, il est préférable d’écrire avec le maximum de retour à la ligne.
« View - Wrap Lines »
L’affichage est plus clair mais cela ne résout pas le problème que tout le texte est marqué en différence.

5. Dans le fichier « Tarte alsacienne aux myrtilles » les sauts de lignes sont différents. On peut le remarquer dans la barre d’état en bas des fichiers 
 
Côté droit :
Côté gauche : 
« Edit – Options – Compare – Général » Cocher « Ignore carriage return differences »

6. Dans les fichiers « Tarte aux myrtilles » le texte est identique mais avec une casse (majuscule/minuscule) différente.
« Edit – Options – Compare – Général » Cocher « Ignore case »
Quel sont les caractères qui ne sont toujours marqués comme différents ?

7. Dans le fichier « Chique » quelles sont les différences ?


8. Dans le fichier « Quiche lorraine » quelles sont les différences ?
« Edit – Options – Compare – Général » Changer le mode de comparaison de « Whitespace »
Compare :
Ignore Change : 
Ignore all : 

9. Dans le fichier « Salade vosgienne » quelles sont les différences ?
« Edit – Options – Compare – Général » Cocher « Ignore blank lines »

10. Dans le fichier « Vôte » l’encodage des caractères (accentués) sont différents.
 

11. Dans le fichier « Tourte Vosgienne » copier toutes les différences vers la droite
« Copy All to Right »

12 Dans le fichier « Choux à la crème de framboise » quelles sont les différences ?
« Edit – Options – Compare – Général » Cocher « Match similar lines » 

13. Dans le fichier « Pâté lorrain »
- Copier la ligne 7 vers la droite (Alt + flèche droite) 
- Copier la ligne 8 vers la gauche et passer à la différence suivante (Alt + Ctrl + gauche)
- Copier les lignes 21 et 26  vers la droite 
- Copier la ligne 31 vers la gauche
- Copier la ligne 33 vers la gauche

Quitter. WinMerge vous propose de sauvegarder les 2 fichiers.

14. Utiliser WinMerge pour visualiser les changements fait dans le fichier php.ini par rapport au fichier php.ini-development 
