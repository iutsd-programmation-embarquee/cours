---
layout: layouts/page.njk
title: Gestionnaire de code source
---

> Un logiciel de gestion de versions est un logiciel qui permet de stocker un ensemble de fichiers en conservant la chronologie de toutes les modifications qui ont été effectuée. Il permet notamment de retrouver les différentes versions d'un lot de fichiers.

Les logiciels de gestion de versions sont utilisés notamment en développement logiciel pour conserver le code source relatif aux différentes versions d'un logiciel.

Il existe plusieurs systèmes comme : CVS, SVN, Microsoft Team Foundation, Bazaar, Mercurial, et Git.

## GIT

![Git](img/git.png)

Git est un logiciel de gestion de versions décentralisé. Il a été créé par Linus Torvalds, auteur entre autres du noyau Linux ! C'est actuellement le système le plus populaire.
Git fonctionne de manière extrêmement efficace sur les fichiers texte mais il moins performant sur les fichiers binaires (jpeg, mp3, mp4, …) que svn par exemple.

## Dépôts distants

Plusieurs fournisseurs proposent d'héberger vos dépôts sur Internet permettant de partager et distribuer votre code avec plusieurs personnes.
On trouve : GitLab, Github (Microsoft), Bitbucket (Atlassian). Chacun possède ses avantages et ses inconvénients. Les plateformes ajoutent des outils supplémentaires en plus de la gestion de version comme : de la gestion de projet, des wiki, des gestionnaires de rapport de bugs, des tests automatiques, …
GitLab possède une version communautaire que l'on peut installer sur son propre serveur, de plus la création de dépôts privés sont autorisés pour les comptes gratuits.

## Concepts

Avant toute chose, il convient de configurer les informations vous concernant. Utiliser l'email utilisé pour l'inscription sur GitLab. Ces informations sont importantes car toutes les validations dans Git utilisent cette information et elle est indélébile dans toutes les validations que vous pourrez réaliser.

```shell-session
$ git config --global user.name "<VOTRE NOM>"
$ git config --global user.email <VOTRE EMAIL>@univ-lorraine.fr
```

***Dépôt :*** Un dépôt Git est un entrepôt virtuel pour un projet. Il permet d'enregistrer l'historique des changements et les différentes versions du code et d'y accéder au besoin.

***Working directory :*** C’est le répertoire de travail. Il contient toutes les sources de votre projet susceptibles d’être dans le dépôt. C’est depuis le répertoire de travail que l’on effectue les modifications souhaitées sur les fichiers.

***dossier .git :*** c’est un dossier à la racine de votre répertoire de travail permettant à Git de sauvegarder l’état actuel et toute l’historique du dépôt. (C’est un dossier caché il est nécessaire de modifier les options de l’explorateur Windows pour le voir)

Pour initialiser un dépôt, se placer dans le dossier contenant le projet, puis

```shell-session
$ git init
```

Lorsque vous exécutez cette commande, un nouveau sous-répertoire `.git` est créé dans le répertoire de travail. De même, une branche principale est créée.

***staging area :*** zone de transit, elle contient les fichiers et modifications apportés qui sont en attente d'une validation et d’un enregistrement.
Ajouter un fichier dans la zone de transit.
git add monfichier.html
Ajouter tous les fichiers du dossier courant (symbolisé par un point .) et détectés comme modifiés dans la zone de transit

```shell-session
$ git add .
```

Retirer le fichier `monfichier.html` de la zone de transit

```shell-session
$ git reset monfichier.html
```

Liste l’état de la zone de transit et les fichiers de répertoire de travail qui ont été modifiés.

```shell-session
$ git status
```

Annule les modifications d’un fichier pour retrouver tel qu’il était lors de la dernière validation

```shell-session
$ git restore monfichier.html
```

## Branches

Une branche est un pointeur vers un commit, elle permet d'enregistrer des modifications de manière parallèle à une autre branche. La branche principale s'appelle master.

Lister les branches, l'étoile indique la branche courante.

```shell-session
$ git branch -v
```

Créer une nouvelle branche

```shell-session
$ git branch <nom-branche>
```

Se positionner sur une branche

```shell-session
$ git checkout <nom-branche>
```

Créer une nouvelle branche et se positionner dessus

```shell-session
$ git checkout -b <nom-branche>
```

Fusionner les modifications contenues dans une branche vers la branche courante

```shell-session
$ git merge <nom-branche>
```

Valide les changements qui sont dans la zone de transit. Chaque enregistrement doit obligatoirement être accompagné d’un message explicite indiquant les changements effectués. Le commit est identifié par une empreinte numérique unique (hash)

```shell-session
$ git commit "Message descriptif des modifications"
```

Lister tous les commit contenus dans le dépôt

```shell-session
$ git log
```

![Messages](https://imgs.xkcd.com/comics/git_commit.png)

## Dépôts distants
Les dépôts distants sont des copies de votre dépôt qui sont hébergé sur un serveur. Ils permettent de partager son dépôt avec d'autres personnes. Par convention le dépôt distant principal est appelé origin.

```shell-session
$ git remote add origin https://gitlab.com/<VOTRE COMPTE>/<PROJET>.git
```

Affiche la liste des dépôts distants

```shell-session
$ git remote -v
```

Récupérer les modifications du dépôt distant origin.

```shell-session
$ git fetch origin
```

Récupérer les modifications de la branche master du dépôt distant origin et les fusionner avec la branche locale. (fetch + merge)

```shell-session
$ git pull origin master
```

Initialiser un dépôt depuis un dépôt distant, équivalent à git init puis git pull

```shell-session
$ git clone https://gitlab.com/<VOTRE COMPTE>/<PROJET>.git
```

### Utiliser une clé SSH avec un dépôt distant

> **Travail à faire :**\
> Ajouter la clé de votre compte UL dans les options de connexion de votre compte GitLab.

SCM
: Source Code Management
