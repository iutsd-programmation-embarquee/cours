---
layout: layouts/page.njk
title: NodeJS
---

Avant de commencer assurez vous que Git est présent sur votre système de développement.

Installer le [paquet logiciel](/linux/paquet/) `git`.

Installer le [paquet logiciel](/linux/paquet/) `nodejs`.

## Gestionnaire de paquet

`npm` est le gestionnaire de paquets officiel de Node.js. Il gère les dépendances des paquets pour une application.

Installer le [paquet logiciel](/linux/paquet/) `npm`.


La commande pour installer des paquets et l'intégralité de ses dépendances est

```shell-session
$ npm install paquet
```

Les options :

`--save-prod` ou `-P` ou  _rien_ : Ajoute le paquet comme dépendance du projet. Il est nécessaire à son fonctionnement.\
`--save-dev` ou `-D` : Ajoute le paquet comme dépendance de développement. Le paquet est utilisé pour le développement mais ensuite il n'est pas nécessaire pour son éxecution. Par exemple un compilateur CSS.\
`--save-optional` ou `-O` : Ajoute le paquet comme dépendance optionnelle.\n
`--global` ou `-g` : Ajoute un outil de manière globale pour l'ensemble des projets. On peut ensuite l'utiliser comme une commande dans le terminal. Il est nécessaire d'être `root` ou d'utiliser la commande `sudo`.


https://www.ipgirl.com/286/quelle-est-loption-save-pour-npm-install.html

Yarn est un gestionnaire de paquet alternatif à `npm`. Il se veut plus rapide, totalement compatible avec npm et plus cohérent dans l'installation de différentes versions des paquets.

```shell-session
$ npm install -g yarn
```
Nodemon est un outil qui surveille les changements sur les fichiers sources et relance automatiquement le serveur.

```shell-session
$ npm install -g nodemon
```


## Liste de contrôle

|application|commande|version|
|-|-|-:|
nodejs|node -v|10.21.0
npm   |npm -v |5.8.0
