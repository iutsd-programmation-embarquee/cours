---
layout: layouts/page.njk
title: Docker
---

Liste des images

```shell-session
$ docker images
```

```shell-session
$ docker run --env-file ./env.list image
```

Liste des containeurs et leur état.

```shell-session
$ docker ps -a
```

```shell-session
$ docker exec container env
```

docker create
docker-compose up -d
docker volume ls


Compose est un outil permettant de définir et d'exécuter des applications Docker multi-conteneurs. Avec Compose, vous utilisez un fichier YAML pour configurer les services de votre application. Ensuite, avec une seule commande, vous créez et démarrez tous les services à partir de votre configuration. Pour en savoir plus sur toutes les fonctionnalités de Compose, consultez la liste des fonctionnalités.