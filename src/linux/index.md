---
layout: layouts/page.njk
title: Linux
---

+ Utiliser le [système de fichiers](fs)
+ Installer des [paquets logigicels](paquet)
+ Gérer les [utilisateurs](utilisateurs)
+ Attribuer des [droits](droits)
+ Gérer les [réseaux](network)
+ Gérer les [services](services)


## Commandes utiles

Éteindre (utilisateur `root` ou `sudo`).

```shell-session
$ halt
$ # ou shutdown -h now
```

Redémarrer (utilisateur `root` ou `sudo`).

```shell-session
$ reboot
$ # ou shutdown -r now
```
