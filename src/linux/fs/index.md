---
layout: layouts/page.njk
title: Dossiers et fichiers
---

Les fichier dans un système Linux n'ont pas forcement d'extension. Pour différencier un fichier `cinema` d'un dossier `cinema`, il convient d'ajouter au dossier une barre oblique `cinema/`

Lister les fichiers et dossiers.\
Commande `ls` (list)

```shell-session
ls
```

Lister tous les fichiers et dossiers, même ceux cachés. Les fichiers et dossiers cachés sont ceux dont le nom commence par un point `.`.\
`.cinema\` est un dossier caché.

```shell-session
ls -a
```

Afficher le détail des fichier sous forme de liste.

```shell-session
ls -l
```

Afficher le détail de tous les fichiers sous forme de liste.

```shell-session
ls -la
```

### Dossiers

Créer un dossier `cinema`.\
Commande `mkdir` (make directory).

```shell-session
mkdir cinema
```

Supprimer le dossier `cinema` à condition que celui ci soit vide. \
Commande `rmdir` (remove directory).

```shell-session
rmdir cinema
```

Supprimer tous les fichier et sous dossier, ainsi que le dossier `cinema`. \
Commande `rm --recursive` (remove).\

Attention en ligne de commande il n'y a pas de corbeille pour récupérer des fichiers accidentellement supprimés.

```shell-session
rm -r cinema/
```

#### Renommer ou déplacer

Le renommage est équivalent à déplacer un fichier dans le même dossier avec un nom différent.\
Commande `mv` (move).

```shell-session
mv index.js server.js
mv main.css styles/
```

#### Copier

```shell-session
cp index.js server.js
cp main.css styles/
```

#### Analyser l'occupation de la carte mémoire.

```shell-session
ncdu
```

fs
: File System
