---
layout: layouts/page.njk
title: XML
---

XML permet de définir la structure du document uniquement, ce qui permet d'une part de pouvoir définir séparément la présentation de ce document, d'autre part d'être capable de récupérer les données présentes dans le document pour les utiliser.

Toutefois la récupération des données encapsulées dans le document nécessite un outil appelé **analyseur syntaxique** (en anglais **parser**), permettant de parcourir le document et d'en extraire les informations qu'il contient.

## XML bien formé

Un document est appelé "document XML" s'il est bien formé, ce qui signifie qu'il respecte les règles syntaxiques de XML.
-	Les documents XML doivent avoir un élément racine ;
-	Les éléments XML doivent avoir une balise de fermeture ;
-	Les balises sont "case sensitive" ;
-	les éléments XML doivent être positionnés correctement ;
-	Les valeurs d'attribut XML doivent toujours être entre guillemets.

## XML valide

Un document XML est valide si, en plus, il respecte la grammaire du langage, contenue dans un fichier appelé **DTD** (Définition de Type de Document).

La Document Type Definition (DTD), ou Définition de Type de Document, est un document permettant de décrire un modèle de document SGML ou XML. Le modèle est décrit comme une grammaire de classe de documents : grammaire parce qu'il décrit la position des termes les uns par rapport aux autres, classe parce qu'il forme une généralisation d'un domaine particulier, et document parce qu'on peut former avec un texte complet.

## Analyseur

L'analyseur syntaxique (généralement francisé en parseur) est un outil logiciel permettant de parcourir un document et d'en extraire des informations. 

Dans le cas de XML (on parle alors de parseur XML), l'analyseur permet de créer une structure hiérarchique contenant les données contenues dans le document XML.

On distingue deux types de parseurs XML :

-	les **parseurs validants** (validating) permettant de vérifier qu'un document XML est conforme à sa DTD (valide)
-	les **parseurs non validants** (non-validating) se contentant de vérifier que le document XML est bien formé (c'est-à-dire respectant la syntaxe XML de base)

Les analyseurs XML sont également divisés selon l'approche qu'ils utilisent pour traiter le document. On distingue actuellement deux types d'approches :

Les API utilisant une approche **hiérarchique** : les analyseurs utilisant cette technique construisent une structure hiérarchique contenant des objets représentant les éléments du document, et dont les méthodes permettent d'accèder aux propriétés. La principale API utilisant cette approche est **DOM** (Document Object Model)

Les API basés sur un mode **événementiel** permettent de réagir à des événements (comme le début d'un élément, la fin d'un élément) et de renvoyer le résultat à l'application utilisant cette API. **SAX** (Simple API for XML est la principale interface utilisant l'aspect événementiel

Ainsi, on tend à associer l'approche hiérarchique avec DOM et l'approche événementielle avec SAX.

## DOM (Document Object Model)

DOM (Document Object Model, traduisez modèle objet de document) est une spécification du W3C (World Wide Web Consortium) définissant la structure d'un document sous forme d'une hiérarchie d'objets, afin de simplifier l'accès aux éléments constitutifs du document.

Plus exactement DOM est un langage normalisé d'interface (API, Application Programming Interface), indépendant de toute plateforme et de tout langage, permettant à une application de parcourir la structure du document et d'agir dynamiquement sur celui-ci. Ainsi Javascript et ECMAScript utilisent DOM pour naviguer au sein du document HTML, ce qui leur permet par exemple de pouvoir récupérer le contenu d'un formulaire, le modifier, ...

## SAX (Simple API for XML)

SAX est une API basée sur un modèle événementiel, cela signifie que SAX permet de déclencher des événements au cours de l'analyse du document XML. 
Une application utilisant SAX implémente généralement des gestionnaires d'événements, lui permettant d'effectuer des opérations selon le type d'élément rencontré.

Ainsi, une application basée sur SAX peut gérer uniquement les éléments dont elle a besoin sans avoir à construire en mémoire une structure contenant l'intégralité du document.

L'API SAX définit les quatre interfaces suivantes :

DocumentHandler possèdant des méthodes renvoyant des événements relatifs au document :
-	startDocument() renvoyant un événement lié à l'ouverture du document
-	startElement() renvoyant un événement lié à la rencontre d'un nouvel élément
-	characters() renvoyant les caractères rencontrés
-	endElement() renvoyant un événement lié à la fin d'un élément
-	endDocument() renvoyant un événement lié à la fermeture du document


ErrorHandler possèdant des méthodes renvoyant des événements relatifs aux erreurs ou aux avertissements
DTDHandler renvoie des événements relatifs à la lecture de la DTD du document XML
EntityResolver permet de renvoyer une URL lorsqu'une URI est rencontrée

## Comparaisons de DOM et de SAX

Les analyseurs utilisant l'interface DOM souffrent du fait que cette API impose de construire un arbre en mémoire contenant l'intégralité des éléments du document en mémoire quelle que soit l'application. Ainsi pour de gros documents (dont la taille est proche de la quantité de mémoire présente sur la machine) DOM devient insuffisant. De plus, cela rend l'utilisation de DOM lente, c'est la raison pour laquelle la norme DOM est généralement peu respectée car chaque éditeur l'implémente selon ses besoins, ce qui provoque l'apparition de nombreux parseurs utilisant des interfaces propriétaires...

Ainsi de plus en plus d'applications se tournent vers des API événementielles telles que SAX, permettant de traiter uniquement ce qui est nécessaire.

## Parseur XML sur iOS

NSXMLParser c’est le parseur inclut par défaut dans l’iPhone SDK d’Apple. Il est écrit en Objective. C’est un parseur SAX.

libxml2 est une librairie Open Source qui est inclut par défaut dans l’iPhone SDK.. C’est une librairie écrite en C. C’est entierement compatible avec l’Objective C mais un plus dificile à utiliser car il n’y a pas tous les objets pratiques d’Objective C (NSDictionnary, NSArray, NSString). Il faut être vigilant lors de la manipulation des chaine de caractères et des pointeurs. La librairie supporte à la fois les traitement DOM et SAX. Le parseur SAX est trés performant car il peut traiter le fichier XML pendant que celui-ci est en train d’être téléchargé sans attendre la fin du fichier.

TBXML est un parseur DOM léger conçu pour être rapide tout en utilisant le moins de mémoire possible. En contre partie il ne valide pas l’XML, ne supporte pas Xpath est ne fonctionne quand lecture seulement.

TouchXML est un parseur DOM basé sur NSXML style DOM XM. Comme TBXML, il travail en lecture seulement mais supporte XPath.

KissXML est un autre parseur DOM basé sur TouchXML. Il ajoute la possibilité d’édition du document XML ainsi que sa sauvegarde (écriture) .

TinyXML est un parseur DOM écrit en C. Il consiste en seulement 4 fichiers C et 2 fichier h. Il supporte la lecture et l’écriture des documents XML, mais ne supporte pas Xpath, sauf à rajouter TinyXPath.

GDataXML est encore un autre parseur DOM basé sur NSXML. Il est développé par Google comme partie de leur bibliothèque cliente Objective-C. Il consiste dans un seul fichier .m et .h. il supporte la lecure et l’écriture et les requêtes XPath.
Chaque bibliothèque possède ses avantages et ses inconvénient au niveau de la difficulté d’utilisation, de la vitesse et de la consommation mémoire.

 
Ce genre de test est à considérer avec précaution car normalement les parseur SAX sont conçu pour normalement être plus rapides.

 

Les parseurs SAX consomment moins de mémoire. 

XPath

XPath est un langage (non XML) pour localiser une portion d'un document XML
Une expression XPath est un chemin de localisation, constitué de pas de localisation. Les pas de localisation sont séparés par le caractère « / ». Un chemin ressemble ainsi au chemin dans un système de fichiers.

article[1]/auteurs/auteur[2]	
sélectionne le second auteur (Dubois) du premier article

article[ count( article/auteurs/auteur) >1 ]	
sélectionne les articles qui ont au moins 2 auteurs

XSLT

XSLT (eXtensible Stylesheet Language Transformations) est un langage de transformation XML de type fonctionnel. Il permet notamment de transformer un document XML dans un autre format, tel PDF ou encore HTML pour être affiché comme une page web.
L’objectif principal est la transformation d’un xml vers un autre xml (définit par un autre schéma). Mais on peut l’utiliser pour faire des transformations dans des documents non XML.
