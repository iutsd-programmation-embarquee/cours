---
layout: layouts/page.njk
title: JSON
date: Last Modified
---

Un document JSON comprend deux types d'éléments structurels :
- des ensembles de paires « propriété » (alias « clé ») / « valeur »
- des listes ordonnées de valeurs

Ces mêmes éléments représentent trois types de données :
- des objets
- des tableaux
- des valeurs génériques de type booléen, nombre, chaîne de caractères, date, tableau imbriqué,
objet imbriqué.

```javascript
{
  "prenom": "Albert",
  "nom": "Einstein",
  "estEmploye": true,
  "age": 27,
  "adresse": {
    "voie": "rue de l'atome",
    "ville": "Saint Dié des Vosges",
    "region": "Grand Est",
    "codePostal": "88100"
  },
  "telephones":
  [
    { "type": "maison", "number": "03 29 55 11 12" },
    { "type": "bureau", "number": "03 29 56 88 99" },
    { "type": "mobile", "number": "06 07 08 54 68" }
  ]
}
```

L'accolade { représente un ensemble de propriétés

Les paires propriétés/valeurs sont séparé par :

Les tableaux sont représentés par un crochet [

JSON
: JavaScript Objet Notation est un format standard utilisé pour représenter des données structurées de façon semblable aux objets du langage Javascript. Il est habituellement utilisé pour transmettre des données vers des sites web ou depuis des services web.

KVP
: Un ensemble Clé-Valeur (Key-Value Pair) représente une données. Elle définie d'un côté par le nom de la propriété (clé) et de l'autre par une valeur associée.
