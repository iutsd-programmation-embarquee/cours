---
layout: layouts/page.njk
title: Algèbre de Boole
---

  <style>
    table {
        border: 1px solid #333;
        border-collapse: collapse;
    }
    td, th {
        border: 1px solid #333;
        text-align: center;
        padding: 2px;
    }
    .tab-content { margin-top:0.5em; }
    /* SVG */
    svg {
        font-size:2em;
    }
    text {
        text-anchor: middle;
        pointer-events: none;
        fill: currentColor;
    }
  </style>



La logique binaire est une sous ensemble des mathématiques inventé par George Boole, un mathématicien anglais du 19<sup>e</sup> siècle.

Appelée algèbre de Boole, cette algèbre n'accepte que **deux** valeurs numériques : <strong>0</strong> et <strong>1</strong>.

Suivant le contexte de l'application ces 2 valeurs peuvent prendre les représentations suivantes :

        <table>
            <tr><td>1</td>        <td>0</td></tr>
            <tr><td>&#x22a4;</td>        <td>⊥</td></tr>
            <tr><td>on</td>       <td>off</td></tr>
            <tr><td>allumé</td>   <td>éteint</td>     <td>pour une lampe</td></tr>
            <tr><td>marche</td>   <td>arrêt</td>      <td>pour un moteur</td></tr>
            <tr><td>haut</td>     <td>bas</td></tr>
            <tr><td>positif</td>  <td>négatif</td></tr>
            <tr><td>+5 V</td>     <td>0 V</td>        <td>pour un système électrique</td></tr>
            <tr><td>vrai</td>     <td>faux</td></tr>
            <tr><td>true</td>     <td>false</td></tr>
            <tr><td>oui</td>      <td>non</td></tr>
            <tr><td>yes</td>      <td>no</td></tr>
            <tr><td>ok</td>       <td>ko</td></tr>
            <tr><td>réussite</td> <td>échec</td></tr>
            <tr><td>noir</td>     <td>blanc</td>      <td>pour un dessin sur une feuille de papier</td></tr>
        </table>

En informatique les variables logiques s'appellent des variables booléennes ou des booléens (en : boolean).

## Diagramme de Venn

Les diagrammes de Venn sont des représentations graphiques pour visualiser les opérations booléennes.

<svg id="VennNon" width="202" height="152">
    <rect x="1" y="1" fill="#ff3030" stroke="#000000" stroke-width="2" width="200" height="150"/>
    <circle fill="#fff" stroke="#000000" stroke-width="2" cx="101" cy="76" r="50"/>
    <text x="50%" y="50%" dy="0.3em">a</text>
</svg>

## Les trois opérateurs logiques de base

https://commons.wikimedia.org/wiki/Logic_gates_unified_symbols



### NON

La fonction NON (en : NOT) inverse le résultat, c'est qui est vrai devient faux et ce qui est faux devient vrai.

    <div class="tabs">
    <ul class="nav nav-tabs">
        <li class="is-active"><a href="#NonVerite"     data-toggle="tab">Table de vérité</a></li>
        <li><a href="#NonVenn"       data-toggle="tab">Diagramme de Venn</a></li>
        <li><a href="#NonSymboles"   data-toggle="tab">Symboles</a></li>
        <li><a href="#NonNotations"  data-toggle="tab">Notations</a></li>
        <li><a href="#NonJavascript" data-toggle="tab">Javascript</a></li>
    </ul>
    </div>

    <div class="tab-content">
        <div class="tab-pane active" id="NonVerite">
            <table>
            <colgroup>
                <col style="width:6em">
                <col style="width:6em">
            </colgroup>
                <tr><td>a</td><td>NON(a)</td></tr>
                <tr><td>0</td><td>1</td></tr>
                <tr><td>1</td><td>0</td></tr>
            </table>
        </div>

        <div class="tab-pane" id="NonVenn">
            <svg id="VennNon" width="202" height="152">
                <rect x="1" y="1" fill="#ff3030" stroke="#000000" stroke-width="2" width="200" height="150"/>
                <circle fill="#fff" stroke="#000000" stroke-width="2" cx="101" cy="76" r="50"/>
                <text x="50%" y="50%" dy="0.3em">a</text>
            </svg>
        </div>

		<div class="tab-pane" id="NonNotations">
            <pre>ā      : algèbre de Boole
!a     : Java, Javascript, C#, C++, Objective C ; Swift
NOT(a) : SQL
Non(a) : 4D</pre>
        </div>

		<div class="tab-pane" id="NonSymboles">
            <table>
                <tr><td>
            <svg width="100" height="50" version="1.0">
                <path style="fill:none;stroke:#000000;stroke-width:1.5;stroke-linecap:butt;stroke-linejoin:miter;" d="M 5,25 L 95,25" />
                <path style="fill:#fff;fill-opacity:1;stroke:#000;stroke-width:1.5;stroke-linejoin:miter;marker:none;stroke-opacity:1;visibility:visible;display:inline;overflow:visible;enable-background:accumulate" d="M 30,5 L 30,45 L 70,25 z" />
                <path style="fill:#fff;stroke:#000000;stroke-width:1.5;stroke-linejoin:miter;marker:none;stroke-opacity:1;visibility:visible;display:inline;overflow:visible;enable-background:accumulate" d="M 80,25 A 4,4 0 1 1 72,25 A 4,4 0 1 1 80,25 z" transform="translate(-1,0)" />
            </svg>
            </td><td>
            <svg
            width="100" height="50" version="1.0">
                <path style="fill:none;stroke:#000000;stroke-width:1.5;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" d="m 75,25 h 20"/>
                <path style="fill:none;stroke:#000000;stroke-width:1.5;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" d="M 5,25 h 20"/>
                <rect style="fill:#fff;stroke:#000000;stroke-width:1.5;stroke-linejoin:miter;display:inline;overflow:visible;enable-background:accumulate" width="50" height="40" x="25" y="5" />
                <text style="font-size:18px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-align:center;line-height:100%;text-anchor:end;fill:#000000;stroke:#000000;stroke-width:0.5;stroke-linecap:butt;stroke-linejoin:miter;font-family:DejaVu Serif;"
                x="56.165039" y="24.5">1</text>
                <path style="color:#000000;fill:none;stroke:#000000;stroke-width:1.5;stroke-linejoin:miter;stroke-opacity:1;marker:none;visibility:visible;display:inline;overflow:visible;enable-background:accumulate"
                d="m 75,18 l 12,7" />
            </g>
            </svg>
            </td>
            </tr>
            </table>
        </div>

		<div class="tab-pane" id="NonJavascript">
			<pre><code class="javascript">!false = <script>document.write(!false);</script>;
!true =  <script>document.write(!true);</script>;</code></pre>
        </div>

	</div>


### ET

L'opérateur ET (en : AND) est une fonction à deux opérandes (a et b), qui associe un résultat vrai seulement si les deux opérandes (a et b) ont la valeur vrai.

    <ul class="nav nav-tabs">
        <li><a href="#EtVerite"     data-toggle="tab">Table de vérité</a></li>
        <li><a href="#EtVenn"       data-toggle="tab">Diagramme de Venn</a></li>
        <li><a href="#EtSymboles"   data-toggle="tab">Symboles</a></li>
        <li><a href="#EtNotations"  data-toggle="tab">Notations</a></li>
        <li><a href="#EtJavascript" data-toggle="tab">Javascript</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="EtVerite">
            <table>
                <colgroup>
                    <col style="width:6em">
                    <col style="width:6em">
                    <col style="width:6em">
                </colgroup>
                <tr><td>a</td><td>	b</td><td>	a ET b</td></tr>
                <tr><td>0</td><td>	0</td><td>	0</td></tr>
                <tr><td>0</td><td>	1</td><td>	0</td></tr>
                <tr><td>1</td><td>	0</td><td>	0</td></tr>
                <tr><td>1</td><td>	1</td><td>	1</td></tr>
            </table>
        </div>

        <div class="tab-pane" id="EtVenn">
            <svg id="VennAnd" x="0px" y="0px" width="220px" height="170px" viewBox="0 0 220 170">
                <rect x="10" y="10" fill="#FFFFFF" stroke="#000000" stroke-width="2" width="200" height="150"/>
                <path fill="none" stroke="#000000" stroke-width="2" d="M85,85c0-18.504,10.059-34.648,25-43.294c-7.356-4.257-15.89-6.706-25-6.706
                c-27.614,0-50,22.386-50,50s22.386,50,50,50c9.11,0,17.644-2.449,25-6.706
                C95.059,119.648,85,103.504,85,85z"/>
                <path fill="#ff3030" stroke="#000000" stroke-width="2" d="M135,85c0-18.504-10.059-34.648-25-43.294c-14.941,8.646-25,24.791-25,43.294s10.059,34.648,25,43.294
                C124.941,119.648,135,103.504,135,85z"/>
                <path fill="none" stroke="#000000" stroke-width="2" d="M135,35c-9.11,0-17.644,2.449-25,6.706
                    c14.941,8.646,25,24.791,25,43.294s-10.059,34.648-25,43.294c7.356,4.257,15.89,6.706,25,6.706c27.614,0,50-22.386,50-50
                    S162.614,35,135,35z"/>
                <text x="30%" y="50%" dy="0.3em">a</text>
                <text x="70%" y="50%" dy="0.3em">b</text>
            </svg>
        </div>

        <div class="tab-pane" id="EtSymboles">

        </div>

        <div class="tab-pane" id="EtNotations">

        </div>

        <div class="tab-pane" id="EtJavascript">
            <pre><code class="javascript">false && false = <script>document.write(false && false);</script>;
false && true = <script>document.write(false && true);</script>;
true  && false = <script>document.write(true && false);</script>;
true  && true = <script>document.write(true && true);</script>;
</code></pre>
        </div>
    </div>

En informatique l’opérateur AND utilise généralement le double symbole : a && b

### OU

L'opérateur OU ou OU inclusif (en : OR) est une fonction à deux opérandes (a et b), qui associe un résultat vrai si au moins un des deux opérandes (a ou b) a la valeur vrai.

    <ul class="nav nav-tabs">
        <li><a href="#OuVerite"     data-toggle="tab">Table de vérité</a></li>
        <li><a href="#OuVenn"       data-toggle="tab">Diagramme de Venn</a></li>
        <li><a href="#OuSymboles"   data-toggle="tab">Symboles</a></li>
        <li><a href="#OuNotations"  data-toggle="tab">Notations</a></li>
        <li><a href="#OuJavascript" data-toggle="tab">Javascript</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="OuVerite">

        </div>

        <div class="tab-pane" id="OuVenn">
            <svg id="VennOr" x="0px" y="0px" width="220px" height="170px" viewBox="0 0 220 170">
                <rect x="10" y="10" fill="#FFFFFF" stroke="#000000" stroke-width="2" width="200" height="150"/>
                <path fill="#ff3030" stroke="#000000" stroke-width="2" d="M85,85c0-18.504,10.059-34.648,25-43.294c-7.356-4.257-15.89-6.706-25-6.706
                c-27.614,0-50,22.386-50,50s22.386,50,50,50c9.11,0,17.644-2.449,25-6.706
                C95.059,119.648,85,103.504,85,85z"/>
                <path fill="#ff3030" stroke="#000000" stroke-width="2" d="M135,85c0-18.504-10.059-34.648-25-43.294c-14.941,8.646-25,24.791-25,43.294s10.059,34.648,25,43.294
                C124.941,119.648,135,103.504,135,85z"/>
                <path fill="#ff3030" stroke="#000000" stroke-width="2" d="M135,35c-9.11,0-17.644,2.449-25,6.706
                    c14.941,8.646,25,24.791,25,43.294s-10.059,34.648-25,43.294c7.356,4.257,15.89,6.706,25,6.706c27.614,0,50-22.386,50-50
                    S162.614,35,135,35z"/>
                <text x="30%" y="50%" dy="0.3em">a</text>
                <text x="70%" y="50%" dy="0.3em">b</text>
            </svg>
        </div>

        <div class="tab-pane" id="OuSymboles">

        </div>

        <div class="tab-pane" id="OuNotations">

        </div>

        <div class="tab-pane" id="OuJavascript">

        </div>
    </div>

<table>
	<colgroup>
		<col style="width:6em">
		<col style="width:6em">
		<col style="width:6em">
	</colgroup>
<tr><td>a</td><td>	b</td><td>	a OR b
<tr><td>0</td><td>	0</td><td>	0
<tr><td>0</td><td>	1</td><td>	1
<tr><td>1</td><td>	0</td><td>	1
<tr><td>1</td><td>	1</td><td>	1
</table>

∨

En informatique l’opérateur OR utilise généralement le double symbole : a || b
<pre><code class="java">System.out.println("false || false  = " + (false || false));
System.out.println("false || true   = " + (false || true));
System.out.println("true  || false  = " + (true || false));
System.out.println("true  || true   = " + (true || true));
</code></pre>

<h2>Les autres fonctions</h2>

## OUI

L'opérateur **OUI** est une fonction à un opérande, qui associe un résultat qui a la même valeur que celle de l'opérande.

    <ul class="nav nav-tabs">
        <li><a href="#OuiVerite"     data-toggle="tab">Table de vérité</a></li>
        <li><a href="#OuiVenn"       data-toggle="tab">Diagramme de Venn</a></li>
        <li><a href="#OuiSymboles"   data-toggle="tab">Symboles</a></li>
        <li><a href="#OuiNotations"  data-toggle="tab">Notations</a></li>
        <li><a href="#OuiJavascript" data-toggle="tab">Javascript</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="OuiVerite">

        </div>

        <div class="tab-pane" id="OuiVenn">
            <svg id="VennOui" x="0px" y="0px" width="220px" height="170px" viewBox="0 0 220 170">
                <rect x="10" y="10" fill="#FFFFFF" stroke="#000000" stroke-width="2" width="200" height="150"/>
                <circle fill="#ff3030" stroke="#000000" stroke-width="2" stroke-miterlimit="10" cx="110" cy="85" r="50"/>
                <text x="50%" y="50%" dy="0.3em">a</text>
            </svg>
        </div>

        <div class="tab-pane" id="OuiSymboles">

        </div>

        <div class="tab-pane" id="OuiNotations">

        </div>

        <div class="tab-pane" id="OuiJavascript">

        </div>
    </div>

a	OUI(a)
0	0
1	1

En informatique la fonction OUI n'existe pas telle quelle.
Le fait d'utiliser une assignation (=) dans une fonction logique peut s'apparenter à la fonction OUI
Dans d'autres domaines la fonction OUI correspond à un interrupteur ou un relais. Par exemple un interrupteur à "on" allume une lampe : La fonction entre l'interrupteur et la lampe est OUI.

## OU EXCLUSIF

L'opérateur **OU exclusif** (en : XOR - eXclusive OR) est une fonction à deux opérandes (a et b), qui associe un résultat vrai seulement si **une seule** des deux opérandes à la valeur vrai. En d'autres termes, seulement si les deux opérandes ont des valeurs distinctes.

    <ul class="nav nav-tabs">
        <li><a href="#XorVerite"     data-toggle="tab">Table de vérité</a></li>
        <li><a href="#XorVenn"       data-toggle="tab">Diagramme de Venn</a></li>
        <li><a href="#XorSymboles"   data-toggle="tab">Symboles</a></li>
        <li><a href="#XorNotations"  data-toggle="tab">Notations</a></li>
        <li><a href="#XorJavascript" data-toggle="tab">Javascript</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="XorVerite">
            <table>
                <colgroup>
                    <col style="width:6em">
                    <col style="width:6em">
                    <col style="width:6em">
                </colgroup>
                <tr><td>a</td><td>	b</td><td>	A XOR B</td></tr>
                <tr><td>0</td><td>	0</td><td>	0</td></tr>
                <tr><td>0</td><td>	1</td><td>	1</td></tr>
                <tr><td>1</td><td>	0</td><td>	1</td></tr>
                <tr><td>1</td><td>	1</td><td>	0</td></tr>
            </table>
        </div>

        <div class="tab-pane" id="XorVenn">
            <svg id="VennXOr" x="0px" y="0px" width="220px" height="170px" viewBox="0 0 220 170">
                <rect x="10" y="10" fill="#FFFFFF" stroke="#000000" stroke-width="2" width="200" height="150"/>
                <path fill="#ff3030" stroke="#000000" stroke-width="2" d="M85,85c0-18.504,10.059-34.648,25-43.294c-7.356-4.257-15.89-6.706-25-6.706
                c-27.614,0-50,22.386-50,50s22.386,50,50,50c9.11,0,17.644-2.449,25-6.706
                C95.059,119.648,85,103.504,85,85z"/>
                <path fill="none" stroke="#000000" stroke-width="2" d="M135,85c0-18.504-10.059-34.648-25-43.294c-14.941,8.646-25,24.791-25,43.294s10.059,34.648,25,43.294
                C124.941,119.648,135,103.504,135,85z"/>
                <path fill="#ff3030" stroke="#000000" stroke-width="2" d="M135,35c-9.11,0-17.644,2.449-25,6.706
                    c14.941,8.646,25,24.791,25,43.294s-10.059,34.648-25,43.294c7.356,4.257,15.89,6.706,25,6.706c27.614,0,50-22.386,50-50
                    S162.614,35,135,35z"/>
                <text x="30%" y="50%" dy="0.3em">a</text>
                <text x="70%" y="50%" dy="0.3em">b</text>
            </svg>
        </div>

        <div class="tab-pane" id="XorSymboles">

        </div>

        <div class="tab-pane" id="XorNotations">

        </div>

        <div class="tab-pane" id="XorJavascript">
            <pre><code class="java">false ^ false =  <script>document.write(false ^ false);</script>;
false ^ true  = <script>document.write(false ^ true);</script>;
true  ^ false =  <script>document.write(true ^ false);</script>;
true  ^ true  = <script>document.write(true ^ true);</script>;
</code></pre>
        </div>
    </div>



⊻

En informatique la fonction XOR utilise généralement le symbole : A ^ B

<table>
	<tr><td>a</td><td>	1</td><td>	NON a
	<tr><td>a</td><td>	0</td><td>	a
	<tr><td>a</td><td>	a</td><td>	0
	<tr><td>a</td><td>	NON a</td><td>	1
</table>

## ET INCLUSIF

L'opérateur ET inclusif (appelé aussi coïncidence ou équivalence logique) (en : EQ Logical equality) est une fonction à deux opérandes (a et b), qui associe un résultat vrai seulement si les deux opérandes ont la même valeur.

    <ul class="nav nav-tabs">
        <li><a href="#EqVerite"     data-toggle="tab">Table de vérité</a></li>
        <li><a href="#EqVenn"       data-toggle="tab">Diagramme de Venn</a></li>
        <li><a href="#EqSymboles"   data-toggle="tab">Symboles</a></li>
        <li><a href="#EqNotations"  data-toggle="tab">Notations</a></li>
        <li><a href="#EqJavascript" data-toggle="tab">Javascript</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="EqVerite">
            <table>
                <colgroup>
                    <col style="width:6em">
                    <col style="width:6em">
                    <col style="width:6em">
                </colgroup>
                <tr><td>a</td> <td>b</td> <td>ET-INCLUSIF(A, B)
                <tr><td>0</td> <td>0</td> <td>1
                <tr><td>0</td> <td>1</td> <td>0
                <tr><td>1</td> <td>0</td> <td>0
                <tr><td>1</td> <td>1</td> <td>1
            </table>
        </div>

        <div class="tab-pane" id="EqVenn">
            <svg id="VennEq" x="0px" y="0px" width="220px" height="170px" viewBox="0 0 220 170">
                <rect x="10" y="10" fill="#ff3030" stroke="#000000" stroke-width="2" width="200" height="150"/>
                <path fill="#fff" stroke="#000000" stroke-width="2" d="M85,85c0-18.504,10.059-34.648,25-43.294c-7.356-4.257-15.89-6.706-25-6.706
                c-27.614,0-50,22.386-50,50s22.386,50,50,50c9.11,0,17.644-2.449,25-6.706
                C95.059,119.648,85,103.504,85,85z"/>
                <path fill="#ff3030" stroke="#000000" stroke-width="2" d="M135,85c0-18.504-10.059-34.648-25-43.294c-14.941,8.646-25,24.791-25,43.294s10.059,34.648,25,43.294
                C124.941,119.648,135,103.504,135,85z"/>
                <path fill="#fff" stroke="#000000" stroke-width="2" d="M135,35c-9.11,0-17.644,2.449-25,6.706
                    c14.941,8.646,25,24.791,25,43.294s-10.059,34.648-25,43.294c7.356,4.257,15.89,6.706,25,6.706c27.614,0,50-22.386,50-50
                    S162.614,35,135,35z"/>
                <text x="30%" y="50%" dy="0.3em">a</text>
                <text x="70%" y="50%" dy="0.3em">b</text>
            </svg>
        </div>

        <div class="tab-pane" id="EqSymboles">

        </div>

        <div class="tab-pane" id="EqNotations">

        </div>

        <div class="tab-pane" id="EqJavascript">

        </div>
    </div>



∧

La fonction ET inclusif et l'inverse de la fonction OU exclusif elle est appelé également XNOR (en : NOT XOR). Cette fonction permet de tester l'équivalence ou la stricte égalité de deux variables.

En informatique la fonction d'équivalence utilise généralement le double symbole : A == B.
Elle peut s'écrire aussi ! (a ^ b) pour NOT XOR.

<pre><code class="java">System.out.println("false == false = " + (false == false));
System.out.println("false == true  = " + (false == true));
System.out.println("true == false  = " + (true == false));
System.out.println("true == true   = " + (true == true));
</code></pre>

## Fonctions universelles

### NON-ET

L'opérateur **NON-ET** (NAND : NOT AND en anglais) est une fonction à deux opérandes, qui associe un résultat vrai si au moins une des deux opérandes à la valeur faux.

    <ul class="nav nav-tabs">
        <li><a href="#NandVerite"     data-toggle="tab">Table de vérité</a></li>
        <li><a href="#NandVenn"       data-toggle="tab">Diagramme de Venn</a></li>
        <li><a href="#NandSymboles"   data-toggle="tab">Symboles</a></li>
        <li><a href="#NandNotations"  data-toggle="tab">Notations</a></li>
        <li><a href="#NandJavascript" data-toggle="tab">Javascript</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="NandVerite">
            <table>
                <colgroup>
                    <col style="width:6em">
                    <col style="width:6em">
                    <col style="width:6em">
                </colgroup>
            <tr><td>a</td><td>B</td><td>NON-ET(A, B)</td></tr>
            <tr><td>0</td><td>	0</td><td>	1</tr>
            <tr><td>0</td><td>	1</td><td>	1</tr>
            <tr><td>1</td><td>	0</td><td>	1</tr>
            <tr><td>1</td><td>	1</td><td>	0</tr>
            </table>
        </div>

        <div class="tab-pane" id="NandVenn">
            <svg id="VennNand" x="0px" y="0px" width="220px" height="170px" viewBox="0 0 220 170">
                <rect x="10" y="10" fill="#ff3030" stroke="#000000" stroke-width="2" width="200" height="150"/>
                <path fill="#ff3030" stroke="#000000" stroke-width="2" d="M85,85c0-18.504,10.059-34.648,25-43.294c-7.356-4.257-15.89-6.706-25-6.706
                c-27.614,0-50,22.386-50,50s22.386,50,50,50c9.11,0,17.644-2.449,25-6.706
                C95.059,119.648,85,103.504,85,85z"/>
                <path fill="#fff" stroke="#000000" stroke-width="2" d="M135,85c0-18.504-10.059-34.648-25-43.294c-14.941,8.646-25,24.791-25,43.294s10.059,34.648,25,43.294
                C124.941,119.648,135,103.504,135,85z"/>
                <path fill="#ff3030" stroke="#000000" stroke-width="2" d="M135,35c-9.11,0-17.644,2.449-25,6.706
                    c14.941,8.646,25,24.791,25,43.294s-10.059,34.648-25,43.294c7.356,4.257,15.89,6.706,25,6.706c27.614,0,50-22.386,50-50
                    S162.614,35,135,35z"/>
                <text x="30%" y="50%" dy="0.3em">a</text>
                <text x="70%" y="50%" dy="0.3em">b</text>
            </svg>
        </div>

        <div class="tab-pane" id="NandSymboles">

        </div>

        <div class="tab-pane" id="NandNotations">

        </div>

        <div class="tab-pane" id="NandJavascript">

        </div>
    </div>



&#x22bc;

En informatique la fonction NON-ET n'existe pas telle quelle, on utilise la combinaison des 2 fonctions NON et ET : ! (A & B)
Par contre en électronique la fonction NON-ET est omniprésente cela est dû à son caractère dit « universel », car elle permet à elle seule de reconstituer toutes les autres fonctions logiques.

NON(A)	NON-ET(A, A)	NON-ET(A)
OU(A, B)	NON-ET( NON-ET(A), NON-ET(B) )
ET(A, B)	NON-ET(NON-ET(A, B))
XOR(A, B)	NON-ET(NON-ET(NON-ET(A), B), NON-ET(A, NON-ET(B)))
Une combinaison de circuits NAND permet d'obtenir toutes les autres fonctions.

### NON-OU

L'opérateur NON-OU (NOR : NOT OR en anglais) est une fonction à deux opérandes, qui associe un résultat vrai seulement si les deux opérandes ont la valeur faux.

    <ul class="nav nav-tabs">
        <li><a href="#NorVerite"     data-toggle="tab">Table de vérité</a></li>
        <li><a href="#NorVenn"       data-toggle="tab">Diagramme de Venn</a></li>
        <li><a href="#NorSymboles"   data-toggle="tab">Symboles</a></li>
        <li><a href="#NorNotations"  data-toggle="tab">Notations</a></li>
        <li><a href="#NorJavascript" data-toggle="tab">Javascript</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="NorVerite">
            <table>
                <colgroup>
                    <col style="width:6em">
                    <col style="width:6em">
                    <col style="width:6em">
                </colgroup>
            <tr><td>a</td><td>	b</td><td>	NON-OU(A, B)
            <tr><td>0</td><td>	0</td><td>	1
            <tr><td>0</td><td>	1</td><td>	0
            <tr><td>1</td><td>	0</td><td>	0
            <tr><td>1</td><td>	1</td><td>	0
            </table>
        </div>

        <div class="tab-pane" id="NorVenn">
            <svg id="VennNor" x="0px" y="0px" width="220px" height="170px" viewBox="0 0 220 170">
                <rect x="10" y="10" fill="#ff3030" stroke="#000000" stroke-width="2" width="200" height="150"/>
                <path fill="#fff" stroke="#000000" stroke-width="2" d="M85,85c0-18.504,10.059-34.648,25-43.294c-7.356-4.257-15.89-6.706-25-6.706
                c-27.614,0-50,22.386-50,50s22.386,50,50,50c9.11,0,17.644-2.449,25-6.706
                C95.059,119.648,85,103.504,85,85z"/>
                <path fill="#fff" stroke="#000000" stroke-width="2" d="M135,85c0-18.504-10.059-34.648-25-43.294c-14.941,8.646-25,24.791-25,43.294s10.059,34.648,25,43.294
                C124.941,119.648,135,103.504,135,85z"/>
                <path fill="#fff" stroke="#000000" stroke-width="2" d="M135,35c-9.11,0-17.644,2.449-25,6.706
                    c14.941,8.646,25,24.791,25,43.294s-10.059,34.648-25,43.294c7.356,4.257,15.89,6.706,25,6.706c27.614,0,50-22.386,50-50
                    S162.614,35,135,35z"/>
                <text x="30%" y="50%" dy="0.3em">a</text>
                <text x="70%" y="50%" dy="0.3em">b</text>
            </svg>
        </div>

        <div class="tab-pane" id="NorSymboles">

        </div>

        <div class="tab-pane" id="NorNotations">

        </div>

        <div class="tab-pane" id="NorJavascript">

        </div>
    </div>


En informatique la fonction NON-OU n'existe pas telle quelle, on utilise la combinaison des 2 fonctions NON et OU : ! (A | B)
La fonction NON-OU, comme la fonction NON-ET, est dite « universelle », car elle permet à elle seule de reconstituer toutes les autres fonctions logiques.
NON(A)	NON-OU(A, A)	NON-OU(A)
OU(A, B)	NON-OU( NON-OU(A, B) )
ET(A, B)	NON-OU(NON-OU(A), NON-OU(B))

## Autres fonctions

### IMPLICATION

La fonction Implication(IMP) est un opérateur à deux opérandes, qui associe un résultat vrai si A est vrai et B est vrai aussi (A entraine B ou B suit A). Lorsque A est faux, B n'est plus entrainé par A, il est donc vrai (juste) que B prenne n'importe quelle valeur.

    <ul class="nav nav-tabs">
        <li><a href="#ImpVerite"     data-toggle="tab">Table de vérité</a></li>
        <li><a href="#ImpVenn"       data-toggle="tab">Diagramme de Venn</a></li>
        <li><a href="#ImpSymboles"   data-toggle="tab">Symboles</a></li>
        <li><a href="#ImpNotations"  data-toggle="tab">Notations</a></li>
        <li><a href="#ImpJavascript" data-toggle="tab">Javascript</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="ImpVerite">
            <table>
                <colgroup>
                    <col style="width:6em">
                    <col style="width:6em">
                    <col style="width:6em">
                </colgroup>
                <tr><td>a</td> <td>b</td> <td>IMP(A, B)
                <tr><td>0</td> <td>0</td> <td>1</td> <td>	B ne suit pas A : c'est vrai
                <tr><td>0</td> <td>1</td> <td>1</td> <td>	B ne suit pas A : c'est vrai
                <tr><td>1</td> <td>0</td> <td>0</td> <td>	B suit A : c'est faux
                <tr><td>1</td> <td>1</td> <td>1</td> <td>	B suit A : c'est vrai
            </table>
        </div>

        <div class="tab-pane" id="ImpVenn">

        </div>

        <div class="tab-pane" id="ImpSymboles">

        </div>

        <div class="tab-pane" id="ImpNotations">

        </div>

        <div class="tab-pane" id="ImpJavascript">

        </div>
    </div>



L’implication est une notion complexe à comprendre, ses fondements proviennent de la logique théorique et des mathématiques.
Néanmoins elle est utilisée en informatique, par l’intermédiaire de la combinaison des opérateurs NON et OU : ! a | b et surtout dans la réalisation des tests unitaires.

Tests unitaires

Les tests unitaires sont du code informatique qui a pour but de vérifier le bon fonctionnement des programmes.
Par exemple, prenons un programme qui manipule deux valeurs A et B.
A représente la valeur de « J'habite en France métropolitaine » et B la valeur de « J'habite en Europe ». Le programme est juste s’il y a implication de A vers B, le test unitaire passe en revue toutes les combinaisons possibles et vérifie que le programme est juste.

- Si j'habite en France est vrai et j'habite en Europe est vrai alors l'implication est vraie, le test est réussi
- Si j'habite en France est vrai et j'habite en Europe est faux alors l'implication est fausse, je ne peux pas habiter en France et pas en Europe, le test échoue, le programme comporte une anomalie.
- Si j'habite en France est faux et j'habite en Europe est vrai alors l'implication est vraie, je peux habiter dans un pays d'Europe autre que la France, le test réussi, le programme est à priori correct, on ne peut pas conclure qu'il soit faux.
- Si j'habite en France est faux et j'habite en Europe est faux alors l'implication est vraie, j'habite dans un pays en dehors de l'Europe. Le test est réussi, le programme est juste.

### INHIBITION

La fonction Inhibition (INH) est un opérateur à deux opérandes,
qui associe un résultat faux si B est vrai et un résultat égal à A si B est faux.
L'opérande A est inhibée (masquée) par B.

    <ul class="nav nav-tabs">
        <li><a href="#InhVerite"     data-toggle="tab">Table de vérité</a></li>
        <li><a href="#InhVenn"       data-toggle="tab">Diagramme de Venn</a></li>
        <li><a href="#InhSymboles"   data-toggle="tab">Symboles</a></li>
        <li><a href="#InhNotations"  data-toggle="tab">Notations</a></li>
        <li><a href="#InhJavascript" data-toggle="tab">Javascript</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="InhVerite">
            <table>
                <colgroup>
                    <col style="width:6em">
                    <col style="width:6em">
                    <col style="width:6em">
                </colgroup>
                <tr><td>a	b	INH(A, B)
                <tr><td>0</td><td>	0</td><td>	0</td><td>	= A
                <tr><td>0</td><td>	1</td><td>	0</td><td>	faux car B vrai
                <tr><td>1</td><td>	0</td><td>	1</td><td>	= A
                <tr><td>1</td><td>	1</td><td>	0</td><td>	faux car B vrai
            </table>
        </div>

        <div class="tab-pane" id="InhVenn">

        </div>

        <div class="tab-pane" id="InhSymboles">

        </div>

        <div class="tab-pane" id="InhNotations">

        </div>

        <div class="tab-pane" id="InhJavascript">

        </div>
    </div>

<svg id="VennNon" width="202" height="152">
    <rect x="1" y="1" fill="#ff3030" stroke="#000000" stroke-width="2" width="200" height="150"/>
    <circle fill="#fff" stroke="#000000" stroke-width="2" cx="101" cy="76" r="50"/>
    <text x="50%" y="50%" dy="0.3em">a</text>
</svg>

<svg id="VennNon" width="202" height="152">
    <rect x="1" y="1" fill="#ff3030" stroke="#000000" stroke-width="2" width="200" height="150"/>
    <circle fill="#fff" stroke="#000000" stroke-width="2" cx="101" cy="76" r="50"/>
    <text x="50%" y="50%" dy="0.3em">a</text>
</svg>

<svg id="VennNon" width="202" height="152">
    <rect x="1" y="1" fill="#ff3030" stroke="#000000" stroke-width="2" width="200" height="150"/>
    <circle fill="#fff" stroke="#000000" stroke-width="2" cx="101" cy="76" r="50"/>
    <text x="50%" y="50%" dy="0.3em">a</text>
</svg>




En informatique l'inhibition s'écrit avec une combinaison des fonctions NON et ET : ! b & a


## Propriétés

Dans l'algèbre de Boole l’opérateur ET est représentée par le symbole · (multiplication) et la fonction OU par le symbole + (addition). Elles reprennent de manière identique à l'algèbre classique, les mêmes propriétés de priorité, commutativité, associativité et distributivité.

### Éléments neutres

La présence d'un élément neutre dans une fonction n'influence pas le résultat final
1 est l'élément neutre de la fonction ET
a · 1 = a
0 est l'élément neutre de la fonction OU
a + 0 = a

## Éléments absorbants
L'élément absorbant de la fonction ET est 0, c'est à dire que la présence d'un seul 0 dans une série de fonctions ET suffit à ce que le résultat soit 0.
A · B · C · D · E = 0 si au moins un seul des éléments est 0
L'élément absorbant de la fonction OU est 1, c'est à dire que la présence d'un seul 1 dans une série de fonctions OU suffit à ce que le résultat soit 1.
A ou B ou C ou D ou E = 1 si au moins un seul des éléments est 1
En informatique cela se traduit par un comportement optimisé des programmes qui s'arrêtent d'évaluer une expression dès qu'ils rencontrent une valeur absorbante. Dans notre premier exemple si A est égal à 0 alors le programme ne cherchera pas à évaluer ni B, ni C, ni D, ni E puisque le résultat final est déjà connu.

## Priorité
La fonction ET est prioritaire sur la fonction OU comme la multiplication l'est sur l'addition.
Soit la formule : a OU b · c avec a et b = 1 et c = 0
- La première opération effectuée prioritairement est b ET c = 1 ET 0 = 0 - La seconde opération est A OU 0 = 1 OU 0 = 1
Le résultat final est 1, si l'on veut prioriser les opérations il faut ajouter des parenthèses comme (A || B) && C
- La première opération effectuée prioritairement est A || B = 1 || 1 = 1 - La seconde opération est 1 && C= 1 && 0 = 0
Le résultat final est cette fois ci 0
a + b · c = a + (b · c)

### Commutativité
a · b = b · a
a + b = b + a

### Associativité
a · b · c = (a · b) · c = a · (b · c)
a + b + c = (a + b) + c = a + (b + c)

## Distributivité
a · (b + c) = (a · b) + (a · c) = a · b + a · c
a + (b · c) = (a + b) · (a + c)
Dans l'algèbre de Boole l’addition est distributive, ce qui n’est pas du tout le cas en arithmétique.

## Théorèmes d’Augustus de Morgan

Propriété de la fonction NON-ET :
<pre><code>NON(a · b) = NON(a) + NON(b)</code></pre>

Propriété de la fonction NON-OU :
<pre><code>NON (A + B) = NON(A) · NON(B)</code></pre>

### Complémentarité
a · NON(a) = 0
a + NON(a) = 1

Involution

Fonction par laquelle chaque élément est l'image de son image
NON ( NON ( a ) ) = a

### Idempotence

La propriété d’idempotence signifie qu'une opération a le même effet appliquée une ou plusieurs fois, ou encore qu'en répétant une fonction plusieurs fois le résultat sera toujours identique à la première itération.

a · a = a · a · a = a · a · a · a = a
a +  a = a +  a + a = a + a + a + a = a

### Inclusion

```
a . b + a . NON B = A
(a + B) . (A + NON B) = A
````
Démonstration : factoriser A :
A . B + A . B = A . (B + B) = A
(A + B) . (A + B) = A + B . B = A


### Théorème d’allégement

```
A . (NON A + B) = A . B
A + NON A . B = A +B
```

Démonstration : utiliser la distributivité (du ET et du OU) :
A . (NON A + B) = A . NON A + A . B = A . B
A + NON A . B = (A + NON A) . (A + B) = A + B

Théorème d’absorption
a . (a + b) = A
a + (a . b) =
