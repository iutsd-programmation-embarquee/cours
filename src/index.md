---
layout: layouts/page.njk
title: Programmation pour systèmes embarqués
---

## Prérequis

+ Connaître les moyens de [contrôle d'accès](security/).
+ Connaître les concepts de la [cryptographie](security/cryptography/).
+ Connaître les différentes méthodes d'[accès sécurisés](security/cryptography/accès).
+ Savoir utiliser un gestionnaire de mots de passe : [KeePass](security/keepass/).
+ Savoir utiliser un [gestionnaire de code source](scm).
+ Maîtriser les bases d'un système d'exploitation [linux](Linux).
+ Connaître Les langages du [web](https://integration-documents-web.netlify.app/) : HTML, CSS, Javascript et SVG

Ce cours fait partie du programme de la licence professionnelle [AMIO](amio)

## Programme

### Installer [Rapsberry Pi OS](system/). <span class='tag geii'>geii</span>

+ Durcir la [sécurité](security/hardened/).

+ [Développer](development/) à distance depuis Windows.

- Donner un accès [SFTP](sftp) aux fichiers pour une mise à jour à distance.

### Conserver des données <span class='tag amio'>amio</span> <span class='tag geii'>geii</span>

+ Installer [PostgreSQL](bdd/postgresql/)

+ Installer [mongodb](bdd/mongodb) <span class='tag geii'>geii</span>

+ Exercice sur le [cinéma](bdd/cinema/)

### Surveiller des données <span class='tag amio'>amio</span>

+ [Surveiller](monitoring/) votre installation.

+ Visualiser les données avec [Grafana](bdd/grafana)

## Héberger une application Web

+ Installer le serveur web [nginx](nginx). <span class='tag geii'>geii</span>

+ Installer le serveur web [apache](apache).

+ Sécuriser la communication avec [HTTPS](security/https) <span class='tag geii'>geii</span> <span class='tag amio'>amio</span>

## Applications

### Héberger une application NodeJS <span class='tag amio'>amio</span>

+ Installer [NodeJS](nodejs/)

+ Créer une API [REST](rest/) avec Express

+ [Documentation](rest/documentation) en OpenAPI et Swagger

+ [GraphQL](rest/graphql)

### Héberger une application avec NodeRed et MongoDB <span class='tag geii'>geii</span>

+ Installer [nodered](nodered)

+ Installer [mongodb](bdd/mongodb)

### Héberger une application .Net <span class='tag geii'>geii</span>

+ Installer [.Net 6](dotnet)

### Contrôler l'accès

+ Installer un système de gestion des [utilisateurs](security/fusionauth)

+ Sécurité avec [jetons](security/jwt)


```mermaid
graph LR

  Navigateur -- 443 --> FusionAuth
  Navigateur -- 443 --> Nginx
  Nginx -- 3000 --> NodeJS
  NodeJS --o Express
  Nginx --> Static
  Express -- 5432 --> bdd[(PostgreSQL)]
```

### Communiquer

+ Faire du Raspberry un [Point d'accès wifi](communication/accesspoint/).

[Bluetooth](communication/bluetooth)

## Colophon

+ Ce site est réalisé avec une technologie [JAMStack](jamstack).
