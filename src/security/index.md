---
layout: layouts/page.njk
title: Contrôle d'accès
---

> ***Définition :*** Le contrôle d'accès à un système d'information par une entité, se décompose en trois sous-processus, l'identification, l'authentification et l'autorisation. 

L'entité peut être une personne ou un logiciel agissant de manière automatique.

## Identification

> ***Objectif :*** demander l'identité d'une personne. 
{.objectif}

Cette identité n'est pas forcément vraie ou authentique. Une carte nationale d'identité par exemple peut être falsifiée, volée et usurpée. Il convient alors à ce qu'un tiers de confiance apporte une preuve irréfutable de l'identité procédant ainsi à l'authentification. 

Un compte utilisateur est une identification.

Prenons l'exemple d'une banque, dont le vigile à l'entrée demanderait la carte d'identité des personnes qui rentrent. L'accès à la banque est public et que n'importe qui peut entrer : employés, clients mais aussi visiteurs venus se renseigner. Mais pour pouvoir entrer il est nécessaire pour des raisons de sécurité de demander l'identité des personnes.

## Authentification

> ***Objectif :*** **vérifier** et de **valider** l'identité d'une personne, **qui** elle est vraiment. 
{.objectif}

Connaitre le mot de passe asocié à un compte utilisateur permet d'authentifié l'utilisateur, de prouver que l'identification correspond bien à la bonne entité.

Le vigile de la banque peut vérifier que la personne ressemble bien à celle sur la photo de la carte d'identité, il peut aussi contrôler les éléments de sécurité comme les hologrammes.

## Autorisation

> ***Objectif :*** vérifier ce à **quoi** vous avez le droit **d'accéder**. Cette autorisation n'est pas forcément lié à l'identité.
{.objectif}

Les employés de la banque ont le droit d'accéder aux bureaux pas les clients. L'appartenance au goupe employé vous donne l'accès aux bureaux.

Les toilette pour femme sont accessible au groupe féminin sans qu'il y ai besoin de vérifier l'identité des personnes.

Par nécessité du secret bancaire l'accès aux coffres est confidentiel et anonyme, le détenteur de la clé ou du code qui qu'il soit peut accéder au coffre.

Dans une application informatique vous vous identifier, puis suivant certaines propriétés que vous possédez vou pouvez accéder ou pas à certaines parties de l'application.

## Services d'authentification

Pendant longtemps les applications monolithiques en PHP ont dominés le domaine des applications web. Les trois processus ont été confondus dans une même base de données et des mêmes procédures. Pourtant ils peuvent être délégués à plusieurs services web extérieurs.

- [FusionAuth](https://fusionauth.io/)
- [Auth0](https://auth0.com/fr)

## Authentification forte

L'authentification forte est une procédure d'authentification qui nécessite l'utilisation d'au moins 2 facteurs de nature différente.

- Ce que je sais (**facteur mémoriel**) : un mot de passe
- Ce que je possède (**facteur matériel**) : Un appareil que je possède, pas forcément celui avec lequel je me connecte. (exemples : téléphone portable, tablette, porte-clefs générateur de tokens)
- Ce que je suis (**facteur corporel**) : la biométrie (exemples : mon empreinte digitale, la reconnaissance faciale avec mon visage ou vocale avec ma voix)
- Ce que je sais faire (**facteur réactionnel**) : la manière de signer.
- Où je me situe (**facteur positionnel**) : localisation de l'endroit où je me connecte.

> Attention : Tout est information, donc tout peut être dupliqué ou reproduit. Une clé physique, peut se résumer au dessin de son profil, elle peut donc être copiée. Une empreinte digitale est aussi un dessin qui peut être reproduit dans du silicone. 
{.warning}

## Authentification unique

> ***Objectif :*** permettre à un utilisateur d'accéder à plusieurs applications différentes en ne procédant qu'à une seule authentification.

L'authentification unique permet de réduire le nombre de combinaison utilisateur / mot de passe à retenir. Elle réduit le sentiment de fatigue de mot de passe, le fait se saisir souvent son mot de passe ou de devoir en connaitre un nombre excessif de mots de passe.

L'inconvénient est que le méchanisme SSO donne accès à de nombreuses ressources une fois l'utilisateur authentifié. Les conséquences peuvent être catastrophiques si une personne mal intentionnée a accès à l'authentification d'un utilisateur. 

SSO
: Single Sign-On 