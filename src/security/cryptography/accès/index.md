---
layout: layouts/page.njk
title: Accès sécurisés
---

## Accès par mot de passe

C'est le moyen le plus connu pour accéder à une ressource protégée. Utiliser un couple nom d'utilisateur et mot de passe.

Pour pouvoir valider le mot de passe le serveur doit comparer le mot de passe de l'utilisateur avec le mot de passe donné. 

Seulement cette méthode possède un inconvénient très important, le mot de passe doit être stocké sur le serveur et donc accessible à toute personne ayant un accès en lecture au serveur.

### Hachage

La solution consiste à stocker sur le serveur le [condensat](index) du mot de passe. Comme la fonction de hachage est non reversible il est impossible de retrouver le mot de passe d'origine avec le condensat.

Condensat stocké sur le serveur : `5af2e7764000bf1a6fc91c8772b3c960`

Mot de passe passé par l'utilisateur : vosges

On applique la fonction de hachage md5() sur la chaine "vosges", si le résultat est identique au condensat stocké sur le serveur le mot de passe est correct.

### Attaque par force brute

Une attaque par force brute consiste à tester une par une toutes les combinaisons de caractères possibles en commençant si possible par des mots communs ou connus pour accélérer la découverte du mot de passe.

Tester toutes les combinaisons possibles nécessite un temps de calcul qui devient de plus en plus grands à mesure que le nombre et la variation des caractères augmentent. 

[Temps de calcul](buteforce.webp) en fonction du nombre de caractère.

Pour éviter des attaques par force brutes il faut que l'utilisateur utilise un mot de passe complexe, mais en utilisant une fonction de hachage lent comme bcrypt le nombre d'essais par seconde est plus faible et donc le temps pour tester l'ensemble des combinaisons est plus long.

### Attaque par dictionnaire

Des compilations de résultats de fonctions de hachage sur des dictionnaires de mots communs sont facilement accessibles publiquement. Précalculée ces tables permettent d'accélérer considérablement le temps de calcul au détriment de la place utilisée pour le stockage de la table.

`5af2e7764000bf1a6fc91c8772b3c960` -> vosges\
`f746e8f07ba1aea5b1aaa07818990f7d` -> volcan\
`f09c2557cb940694f0d9dd8bee52a31f` -> voisin\
`a7f3ea2e41abb87a9800fea7978f18be` -> volaille\
...

Rechercher à l'aide du site https://md5.gromweb.com/ le mot de passe correspond à l'empreinte 768957a081cf27da9c4ddab74aadc7d2

La solution pour l'utilisateur est de choisir un mot de passe qui ne correspond pas à un mot connu dans un dictionnaire (nom commun, prénom, ...)

Générer une table de correspondance entre toutes les combinaisons possibles de lettres suppose des centaines voir des millers de terra octets de données.

### Attaque par Rainbow table

La rainbow table permet de réduire la taille de la table de correspondance. Pour chaque combinaison de caractères on va calculer le hash une première fois comme la table de correspondance classique. Ensuite on va appliquer au condensat une fonction de réduction qui va générer une nouvelle comibaison de mot de passe. On recalcule le condensat de ce nouveau mot de passe puis on recommence l'opération de réduction et de hachage un certain nombre de fois. Seul le dernier condensat est stocké.

Ce dernier condensat défini un ensemble de mots de passe possibles reliés entre eux par les fonctions de réduction. Pour éviter de retomber sur les mêmes combinaisons et les même séquences de mots de passe, les fonctions de réductions sont différentes à chaque étape. En leur attribuant une couleur différente à chacune on obient une table arc-en-ciel.

Pour retrouver un mot de passe à partir d'un condensat il faut réappliquer les fonctions de réduction une par une jusqu'à retrouver une correspondance, le mot de passe à trouver fait partie de cette séquence.

https://mieuxcoder.com/2008/01/02/rainbow-tables/

Quand bien même un mot de passe ne se trouve pas dans une table de correspondance ou un dictionnaire un simple hachage de mot de passe est faillible.

#### analyse fréquentielle

Supposons qu'une personne dispose d'une liste de mots de passe haché de plusieurs millers ou centaines de milliers de peronnes. Si un condensat est utilisé plusieurs fois par différents utilisateurs c'est certainement qu'il a un intérêt particulier. Le pirate portera son attention sur ce mot de passe.

#### répétition

Si un mot de passe haché est utilisé sur différents sites alors il suffit de le compromettre une seule fois pour 

Rechercher à l'aide du site https://md5.gromweb.com/ le mot de passe correspond à l'empreinte 488a0e16cbf8579686dd0916f63a0ec2

### Poivre et sel

Poivrer et saler un mot de passe consiste à ajouter systématique au mot de passe une chaine de caractère afin de dérouter les attaques par dictionnaire.

sel abcd en préfixe : \
voiture -> abcdvoiture -> `972d276e37f9f77bc0377aa4166ee141`

sel wxyz en suffixe :\
voiture -> voiturewxyz -> `37fe5eaa17b45c29bd4f1ea9207d3a5b`

Le même mot de passe n'est pas représenté par le même condensat. L'objectif est atteint.

Il exite 2 manières qui peuvent être combinées.

#### Poivre

Le poivre est une chaine de caractères commune à l'ensemble des mots de passe définie au niveau de l'application. Le poivre est maintenu secret et stocké de manière séparée des informations des utilisateurs.

Il y a tout de même des inconvénients :\
Une fois le poivre défini et utilisé par des comptes on ne peut plus le modifier car ces comptes deviendraient inutilisables.
Si le poivre est découvert alors il suffit de recalculer une seule fois une table de correspondance pour comprometter l'ensemble des mots de passe.

poivre : pznr

compte|hash
---|---
Alice|31dacbace8695aa7e37949a53da4bfb7
Albert|57950c9c9edfd099c5ebd5eb4540b740
Alain|31dacbace8695aa7e37949a53da4bfb7

Alice et Alain utilisent le même mot de passe. Utilisez le site https://www.dcode.fr/hash-md5 pour trouver lequel.

#### Sel

Le sel est une chaine de caractères défini pour un compte particulier chaque mot de passe est modifié par son sel.

L'inconvénient est que le sel est stocké dans la base de données au côté du mot de passe pour pouvoir faire l'opération inverse. Si la base est accessible en lecture alors le sel lui aussi. Cependant pour chaque mot de passe il faut calculer l'intégtralité de la table de correspondance avec le sel spécifié.

compte|sel|hash
---|---|---
Alice|ajdo|554cba47775cba336c0f1dd902b26437
Albert|cbri|2ef380639789ca3ada2d0ee029c30074
Alain|xczo|712f531d2617e162046b3016b32edd00

Alice et Alain utilisent le même mot de passe mais cela ne se remarque pas. Cependant nous avons une indication directe sur le sel utilisé.

Attention multiplier les sels ou les opérations de transformation sur un mot de passe ne le rend pas forcément plus difficile à découvrir. Supposons plusieurs opération de transformation qui consiste à multiplier par 3 puis par 5 puis par 2 au final c'est équivalent à une seule multiplication par 30.

## Accès par clé

Un accès par clé consiste à déposer sur un serveur la clé publique de l'utilisateur. Pour s'authentifier l'utilisateur transmet un message crypté avec sa clé privée. Si le message est authentifié avec la clé publique, alors l'authentification est réussie.

La clé privée reste totalement secrète, elle ne transite jamais sur le réseau et n'est stockée nul part même sous forme condensée sauf sur la machine de l'utilisateur.

> **Travail à faire :**
> 1. Créer un compte sur GitLab.
> 2. Créer une clé personnelle avec PuTTYgen que vous utiliserez pour vos accès distant.
> 3. Ajouter votre clé sur GitLab et dans KeePass.
> 4. Installer l'application FreeOTP sur votre téléphone.
> 5. Activer l'authentification par double facteur sur votre compte GitLab. 
{.devoir}