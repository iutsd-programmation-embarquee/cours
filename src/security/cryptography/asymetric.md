---
layout: layouts/page.njk
title: Chiffrement symétrique
---

> ***Objectif :*** Chiffrer et déchiffrer un message en utilisant une **paire de clé** : une pour chiffrer, une autre pour déchiffrer.
{.objectif}

Le chiffrement asymétrique repose sur l'utilisation d'une **clé publique** (qui est diffusée au monde entier) et d'une **clé privée** (gardée secrète), la clé publique permet de chiffrer le message et la clé privée de le déchiffrer.

A l'inverse, il est aussi possible de chiffrer un message avec sa clé privée et de le déchiffrer avec la clé publique. Comme tout le monde connait la clé publique, le message n'est pas secret, par contre le destinataire est sûr que le message provient de la personne qui possède la clé privée, puisque l'on a réussi à déchiffrer le message.

Pour finir par le plus important : un message chiffré avec la clé publique ne peut plus être déchiffré avec cette clé publique.

Le point fondamental de cette technique est l'impossibilité théorique de déduire la clé privée à partir de la clé publique.

L'inconvénient d'un chiffrement asymétrique c'est qu'il est assez couteux en puissance de calcul.

La clé privée est contenue dans un fichier texte (c'est beaucoup trop long à retenir et à saisir) stocké sur son ordinateur. Il faut **protéger** sa clé privé, car elle doit rester absolument secrète. La clé est chiffré avec un chiffrement symétrique et en choisissant une passphrase (c’est un mot de passe mais en beaucoup plus long).

La clé publique

```
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAACEAgKnaAD11GATtUw/gMulpUnYsOiwbrwHY2z5NndRIjeU=
```

La clé privée associée

```
-----BEGIN RSA PRIVATE KEY-----
MIGoAgEAAiEAgKnaAD11GATtUw/gMulpUnYsOiwbrwHY2z5NndRIjeUCASUCIHm1
bVosTCt6SEenO90h+9cA0kqnRNrVpVl1Y5G4283hAhEA5kE5p27LoEgfbq5M4b7h
kwIRAI8MsZlVeGJ0OKtMAAqLXacCEQCbk9rY6f808nYaWhhFgPlHAhAXMn2qKYkk
uOaYUYN3VN7BAhAcRU1Uf+fRuYaHLF4Gt52u
-----END RSA PRIVATE KEY-----
```

La clé privée chiffrée avec un mot de passe symétrique

```
-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,3EE7D09895310AC9

LbXjSXfivULmnE634ZY5HU1xR6KSMM9YCNoT7LyDVhT5r+v+DTxnl7rjhH3nYLiq
dPylNtUzkHuuIgk7gFwM2MMT92Frh7mQFMWCiV9SHOTEkXNApiOh9BILHG3VX4bb
nxTn6L3VrEMbrcFndivHXmjDMcqX4NDM/Zt+hJujQfTLiEvWY9kVlrEaKHPfZRBQ
HuzT577eMW/gyRblnJt56lpQ6fBaFzcsQvAcgecZqeE=
-----END RSA PRIVATE KEY-----
```

Il n'y a visuellement aucune différence de structure, c'est une suite de chiffres et de lettres. Pourtant une clé est chiffrée, l'autre pas.

-----

### Exercice :

Utilser le site <https://www.devglan.com/online-tools/rsa-encryption-decryption> pour générer une paire de clé publique/privée RSA de 515 bits

Clé publique
```
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAO/rRPjXGcy8I19V3deToOM+d8LaXjli4nDDwxBKmRP/m7aMEMFxc4IHuT/RHj7FSugCDChRwgk01yLJHPVJYvUCAwEAAQ==
```

Clé privée
```
MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEA7+tE+NcZzLwjX1Xd15Og4z53wtpeOWLicMPDEEqZE/+btowQwXFzgge5P9EePsVK6AIMKFHCCTTXIskc9Uli9QIDAQABAkAS/+i4ZCBw7D1I4X3uAoztMhc94RkxJjmUgIxEEARLZv1kKtElK7qSKT0BI7xadLdCQP6d9KNn/HsgTrv7SrJJAiEA/fHthEfYHFVAvJALDKYigbxH1Vf3459TnLVT+feMMDMCIQDx3EkNUGsV+MwoeBkPOBoq/Co9Lei/TsGa3p4QDIDYNwIgVti/Hwcm/IGXcyVb8gZ7NJ08f9Tm3PynLqfAwXMMiYsCIGRKnGBRAJbT3JTLeFi/QtqgRkOoTeNGnTHDLngpNyRxAiEAmgh064kSuOWOB+reNfwitf9fIabDsAMmBfKYCduLnYU=
```

***Étape 1 :*** Chiffrer la phrase `IUT de Saint Dié des Vosges` avec la clé publique.

Message chiffré :

```
g0pg6fXHPvy9hq39M4tOCFxQrxRB0CLbK2wvErWT/Etv+PK2+S1nxOvS/8sKdT4VKfLo6crITkeDnpVrF/MeNQ==
```

***Étape 2 :*** Déchiffrer le message obtenu avec la clé publique.

Cela ne fonctionne pas !

***Étape 3 :*** Déchiffrer le message obtenu avec la clé privée.

Nous retrouvons la phrase originale.

> ***Conclusion :*** un message chiffré avec une clé publique peut être lu uniquement par le possesseur de la clé privée.
{.primary}

***Étape 4 :*** Chiffrer la phrase avec la clé privée.

Message chiffré :

```
i3gs+Dtj5fpyi6XasQiii2j0ADpy3eWP/aP/CT+U1Ql3zmbrTVP3D48zAIg/tNd3dt+ReLgTAW4MBcJOznQZdg==
```

***Étape 5 :*** Déchiffrer le message avec la clé publique.

Nous retrouvons la phrase originale.

***Étape 6 :*** Déchiffrer le message avec la clé privée.

Cela ne fonctionne pas. Mais il n'y a aucun intérêt car une seule personne connait la clé privée.

> ***Conclusion :*** un message chiffré avec une clé privée peut être lu par tous les possesseurs de la clé publique. Le message n'est pas secret car tout le monde connait la clé publique. Mais nous sommes sûr qu'il a été rédigié par celui qui possède la clé privée et lui seulement.
{.primary}
