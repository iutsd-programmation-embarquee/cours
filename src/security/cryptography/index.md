---
layout: layouts/page.njk
title: Cryptographie
---

> ***Définition :*** La **cryptographie** regroupe un ensemble de techniques utilisées pour protéger des données. Elle assure le chiffrement et le déchiffrement des messages afin d'assurer leur confidentialité, authenticité et intégrité. Des fonctions mathématiques complexes associées à des **secrets** ou **clés** sont la base de la cryptographie.
{.definition}

## Chiffrer (Confidentialité)

> ***Objectif :*** S'assurer que le destinataire est le seul à pouvoir lire le message.
{.objectif}

Lorsqu'on envoie une carte postale en dehors d'une enveloppe, le facteur peut lire le message la carte postale.  Lorsque le contenu du courrier est sensible, on le met dans une enveloppe.

La problématique est exactement la même avec le courrier électronique. Les mails traversent un certain nombre de machines (par exemple les serveurs de courriers électroniques de votre fournisseur d'accès et de celui de votre correspondant). Un administrateur d'une de ces machines peut consulter votre courrier électronique. Ceci devient particulièrement gênant le jour où l'on souhaite transmettre des informations sensibles à un correspondant.

Pour du courrier électronique, le mécanisme correspondant à mettre le courrier dans une enveloppe consiste à chiffrer ce message.
Seul le correspondant doit être en mesure de lire ce message.

- [Chiffrement symétrique](symetric)
- [Chiffrement asymétrique](asymetric)

Chaque méthode possède ses forces et ses faiblesses. Le chiffrement symétrique est plus rapide en temps processeur que le chiffrement asymétique.
Dans une connexion ssh l'authentification est assurée par un chiffrement asymétrique car plus sûr, ensuite les 2 parties se mettent d'accord sur une clé de session symétique, unique pour la session en cours, et continuent leurs échanges avec un chiffrement symétrique plus rapide.

La partie privée d'une clé asymétrique est chiffrée de manière symétrique car la clé privée doit rester privée et illisible aux tiers mais tout de même accessible par le possesseur de la clé par un chiffrement connu.

## Signer (Authenticité)

> ***Objectif :*** S'assurer que le message que l'on reçoit est bien envoyé par la personne qui le prétend.
{.objectif}

Quand on envoie un courrier sur lequel on souhaite prouver qu'on est bien la personne qui a écrit ce courrier, on le signe. Le principe est exactement le même avec un courrier électronique.

En temps normal on se fie à l'adresse mail de l'expéditeur pour savoir qui nous l'a envoyé. Malheureusement, et contrairement aux idées reçues, l'adresse mail ne permet pas de garantir que la personne qui a émis le mail est bien celle que l'on croit.  En effet, il est assez simple de _"trafiquer"_ un mail que l'on envoie et de modifier l'adresse d'expéditeur pour se faire passer pour quelqu'un d'autre. Ainsi, l'adresse mail de l'expéditeur ne constitue pas une signature fiable.

## Vérifier (Intégrité)

> ***Objectif :*** S'assurer que le message n'a pas été modifié entre son expéditeur et son destinataire.
{.objectif}

On veut pouvoir être sûr que les données n’ont pas été modifiées depuis leur création. La cryptologie veut prouver qu’il n’y a pas eu falsification du message par l’ajout d’un code d’authentification de message (MAC : Message Authentication Code). Il s’agit d’un algorithme qui crée un petit bloc authentificateur de taille fixe qui est rajouté au message. Le destinataire calcul de son côté le code avec le même algorithme et compare avec le MAC transmis. Si c’est le même alors le message est authentique (non modifié).

Généralement le MAC assure non seulement la vérification de l’intégrité mais aussi de l’authenticité de l’expéditeur, regroupant deux fonctions dans une seule procédure.

## Condenser

> ***Objectif :*** réduire un message à une empreinte unique. De la même manière qu'une empreinte digitale identifie une personne complète.
{.objectif}

Le condensat appelé aussi empreinte, résumé, somme de contrôle ou hash est le résultat de l'application d'une fonction qui pour un ensemble de très grande taille (théoriquement infini) et de nature très diversifiée va renvoyer des résultats aux spécifications précises (en général des chaînes de caractère de taille fixe) optimisées pour des applications particulières.

Les chaînes obtenues permettent d'établir des relations d'égalité, d'égalité probable, de non-égalité et de classement entre les objets de départ, sans accéder directement à ces derniers, en général soit pour des questions d'optimisation (la taille des objets de départ nuit aux performances), soit pour des questions de confidentialité.

Il est **impossible** de reconstituer le message d'origine à partir d'une empreinte.

Selon l'emploi de la fonction de hachage, il est souhaitable :

- Qu'un infime changement de la donnée en entrée (inversion d'un seul bit, par exemple) entraine une perturbation importante de l'empreinte correspondante, rendant une recherche inverse par approximations successives impossible : on parle d'**effet avalanche**.

- Des messages similaires, mais non égaux produisent des empreintes égales ou très légèrement différentes, ces types de résumés permettent par exemple de détecter des plagiats sur les fichiers musicaux.

Les fonctions de hachage sont entre autres : CRC32, MD5, SHA-1, SHA-2

Avec MD5 tous les messages, textes, livres, documents, fichiers sont représentés par une chaine de 32 caractères hexadécimaux (128 bits) quel que soit la taille originale du fichier.

Empreintes MD5 de différents fichiers : 

`bf1c3fc186cd734a19429fea09be3260` : les 7 octets du mot : anneaux,\
`398c89bae2690b4aff938cc23fae4773` : les 1700 pages du livre entier du Seigneur des anneaux,\  
`89d4658a9b326a40abe4a0f733691f03` : les 100 Go du film 4K du Seigneur des anneaux  

Le nombre d'empreintes étant limité par la taille de celle-ci, il est possible de se retrouver face à des collisions, c’est-à-dire des messages différents qui auraient la même empreinte.

MAC
: Message Authentication Code,

Collisation
: Une collision se produit lorsque 2 messages différents ont la même empreinte.

Fingerprint
: litéralement empreinte digitale. Information réduite de taille fixe qui identifie de manière unique un élément plus important.

Effet d'avalanche
: une faible modification de l'entrée génère une modification importante en sortie