---
layout: layouts/page.njk
title: Chiffrement symétrique
---

> ***Objectif :*** Chiffrer et déchiffrer un message en utilisant une **clé partagée unique**.
{.objectif}

Un chiffrement symétrique, appelé aussi chiffrement à clé secrète, utilise la même clé pour chiffrer et déchiffrer le message. Les deux partenaires doivent conserver chacun la clé sans que quiconque d'autre ne la connaisse. Il faut utiliser autant de clés que d'interlocuteurs.

La première communication qui consiste à s'échanger la clé est extrêmement vulnérable à l'écoute par des pirates.
Il existe plusieurs méthodes et algorithmes pour effectuer des chiffrements symétriques : ROT13, DES, 3DES, RC4, BLOWFISH, RIJNDAEL, …

-----

### Exercice 1 :

Le code César est un algorithme qui effectue un décalage sur les lettres de l'alphabet, la clé est la distance de ce décalage.

Algorithme : Décalage de l'alphabet\
Clé : +10
|||||||||||||||||||||||||
-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-
A|B|C|D|E|F|G|H|I|J|**K**|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z
Q|R|S|T|U|V|W|X|Y|Z|**A**|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P


Ce chiffrement est appelé aussi code Avocat (A vaut K).

Déchiffrer ce message :

    VO ZBOCLIDOBO X K BSOX ZOBNE NO CYX MRKBWO XS VO TKBNSX NO CYX OMVKD

Réponse ? :
<https://www.dcode.fr/chiffre-cesar>

:smiley: Vous êtes maintenant devant une phrase énigmatique, un message codé. Le message est visible par tous mais incompréhensible à ceux qui n'en connaissent pas la signification.

-----

### Exercice 2 :

Déchiffrer cet autre message :

```
2wxhNC1FDvrAWgy3wnaahW0RDBUy3+GxIkCPNkP4/j0NzvOreVvpAoEMyYjy8frBweKPJstZpPg5L2l7gPJcDGpsAsl6Pf/FN3k4AzXErJQ+sI28N4HBIIw5rsAcCjnj
```

Algorithme : 3DES (Triple DES)\
Clé : `Cthulhu`

Réponse ? : <https://www.tools4noobs.com/online_tools/decrypt/>

Encore un autre message égnimatique ...
