---
layout: layouts/page.njk
title: Durcir la sécurité
---

> Bonne pratique : Utiliser [KeePass](../keepass) pour gérer vos mots de passe et clés d'accès ssh.

## Utilisateur root

Renseigner un mot de passe pour l'utilisateur `root`. Évite de devoir utiliser `sudo` à chaque train de commandes importantes.

L'utilisation de la commande `sudo` n'est pas forcément une bonne pratique. Sa généralité et son utilisation systèmatique par les utilisateurs ne garantissent pas une absence mauvaise manipulation.

![][sudo]

À lire : [Real sysadmins don't sudo](https://www.redhat.com/sysadmin/sysadmins-dont-sudo).

Lorsque qu'un utilisateur travail quotidiennement sur un ordinateur celui ci, ou les programme qu'il éxecute, ne doivent pas pouvoir endommager le système. Les opérations d'administration nécessite une élévation de privilèges que l'utilisateur peut obtenir à l'aide de la commande `sudo`.

Dans le cas d'un serveur, un utilisateur n'a de toute façon pas accès au système. Lorsque qu'une personne se connecte, c'est forcément un administrateur qui le fait uniquement par nécéssité de tâches d'administration. Il est contre productif d'utiliser en permanence la commande `sudo`.

Le deuxième motif pour activer l'accès root est la modification de longs fichiers de configuration. Si vous n'êtes pas très à l'aise avec l'éditeur nano, nous pouvez vous connecter en ssh avec un navigateur de fichier comme WinSCP, et lancer l'edition sur votre PC avec votre éditeur favoris de n'importe quel fichier se trouvant sur le Raspberry.
Il faut cependant être plus prudent car l'accès est tellement facile que l'on peut oublier que l'on a le pouvoir de tout détruire accidentellement sans aucune restriction.

Utiliser [KeePass](keepass) pou gérer vos mot de passes et clés d'accès ssh.

### Activer l'utilisateur root en lui attribuant un mot de passe.

La précommande `sudo` permet d'attriber temporairement les droits à l'utilisateur `pi`, à effectuer des commandes réservées en temps normal à l'utilisateur `root`.

```shell-session
$ sudo passwd root
```

Changer d'utilisateur (switch user) pour devenir root.

```shell-session
$ su -
```

C'est un raccourci de la commande `su --login root`.

## Connexion par clés

Voir le pargraphe sur la [cryptographie](../cryptography/)

Créer à l'aide du programme PuTTYGen un couple de clés publique/privée qui serviront à remplacer l'authentification par mot de passe.

Cliquer sur Generate et bouger la souris au dessus de la zone vide jusqu'à ce que les clés soient générées.

![][PuttyGen]

Créer un [dossier](/linux/fs) _`.ssh`_ dans le dossier de l'utilisateur `root`

Créer dans ce dossier un fichier _`authorized_keys`_ et copier coller la clé publique dedans.

Mettre le fichier _`authorized_keys`_ en lecture seule

```shell-session
$ chmod -R 700 .ssh
$ chmod 400 .ssh/authorized_keys
```

Mettre un mot de passe et sauvegarder la clé privée. Ajouter la clé privée à [KeePass](keepass).

Donner l'autorisation à l'utilisateur root de se connecter mais uniquement avec une clé ssh. Interdire l'utilisation du mot de passe simple (prohibit-password).

Désactiver les connexions par mot de passe pour tous les utilisateurs. Éditer le fichier `/etc/ssh/sshd_config`.

```properties
PermitRootLogin prohibit-password
PasswordAuthentication no

AllowAgentForwarding yes
AllowTcpForwarding yes
```

Retirer le mot de passe local du compte, il ne sert plus à rien.
:warning: Attention soyez sûr de pouvoir vous connecter par un autre moyen (sudo ou clé ssh) !

```shell-session
passwd -d root
```

Maintenant le compte `root` ne peut plus se connecter avec le couple login/password mais uniquement avec les clés publique/privée.

Utiliser KeePass et KeeAgent pour vous connectez.

Mettre `ssh://ip_raspberry` dans le champ url. KeePass chargera la clé dans l'agent SSH et ouvrira la connexion avec PuTTY.

![][Agent]

[PuTTYGen]: /img/PuTTYGen.png "PuTTY"
[sudo]:https://imgs.xkcd.com/comics/sandwich.png "sudo"
[Agent]: /img/Agent.png "Utilisation de l'agent"

### Se déconnecter automatiquement au bout d'un certain temps d'inactivité.

Éditer le fichier comprenant les préférences de votre interpréteur de commandes `~/.bashrc`.

```properties
export TMOUT=180
```

### Enlever l'utilisateur `pi` du groupe `sudo`.

```shell-session
$ gpasswd -d pi sudo
$ mv /etc/sudoers.d/010_pi-nopasswd /etc/sudoers.d/010_pi-nopasswd.disabled
```

> **Travail à faire :**\
> Utiliser la clé de votre compte UL pour vous connectez à l'utilisateur `pi`\
> Retirer le mot de passe de l'utilisateur `pi`
{.devoir}

## Repousser les personnes indésirables

Dés lors que votre système est ouvert sur Internet un nombre incalculable de personnes malveillantes vont tenter de le pénétrer.

Une ancienne solution mais qui n'est plus efficace aujourd'hui consiste à changer le port d'écoute du serveur ssh qui est le port 22 par défaut. L'utilisation de scanneur de port rend cette protection inefficace. Les pirates ont tôt fait de trouver le nouveau port d'écoute.

### Fail2ban

Installer le [paquet logiciel](/linux/paquet/) `fail2ban`.

Ce script perl scrute les journaux de connexions ssh. Lorsqu'il découvre des multiples essais en provenance d'une ip particulière il la bloque pour un temps déterminé.


Port knocking
: Consiste à ouvrir le port de connexion ssh uniquement lorsqu'une combinaison d'essais sur des ports prédéfinis est effectué par le client.

Élévation de privilèges
: Permet à un utilisateur d'obtenir des privilèges supérieurs à ceux qu'il a normalement.
