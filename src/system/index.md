---
layout: layouts/page.njk
title: Raspberry Pi OS
---

> ***Objectif :***  Installer et utiliser un système d'exploitation pour Raspberry Pi.
{.objectif}

**Raspberry Pi OS** est le système d'exploitation officiel c'est une version aménagée de Debian Linux. La version desktop permet de travailler avec un bureau comme un ordinateur classique. Un clavier, souris et un écran sont indispensables pour l'utiliser.

La version lite ne comporte pas de bureau graphique, il est nécessaire de se connecter à distance à la console pour pouvoir le contrôler.

Pour l'instant Raspberry Pi OS fonctionne en mode 32 bit ou en 64 bits. Vis à vis de la quantité de memoire du modèle 3, la version 64 bits est inutiles, car elle consomme plus d'espace lors de l'adressage et ne permet pas d'accéder à plus de mémoire. Cependant de plus en plus de paquets logiciels sont fournis en mode 64 bits uniquement. Si on veut pouvoir utiliser les dernières versions de ces logiciels, il faut installer la version 64 bits.

Plusieurs autres systèmes pour des usages spécifiques sont disponibles : Ubuntu, OSMC (Media Center), Windows IoT Core (non compatible avec les dernières déclinaisons), PiNet, RiscOs, LibreElec (domotique) etc.


## Copier Raspberry Pi OS (64-bit) Lite

La version Lite est une version allégée, sans interface graphique et sans bureau. Elle convient parfaitement à une utilisation en mode serveur.

Télécharger le fichier _2022-01-28-raspios-bullseye-arm64-lite.img_ sur la [page officielle](https://www.raspberrypi.org/software/operating-systems/)

Transférer l'image sur une carte micro SD avec le logiciel [Rufus](https://github.com/pbatard/rufus/releases/download/v3.18/rufus-3.18p.exe).


### Activer l'accès à distance par ssh

> **headless** : mode de fonctionnement _sans tête_, utilisation d'un appareil sans écran, ni clavier, ni souris branchés directement à lui.

Créer un fichier appelé simplement `ssh`, sans extension. Utiliser notepad, et enregistrer le fichier dans la partition boot de la carte.

Fichier vide, sa seule présence suffit à autoriser les connexions extérieures par ssh.

### Configurer le wifi

Ajouter un fichier `wpa_supplicant.conf` dans la partition boot.

```properties
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=FR
```
Les normes étant différentes suivant les pays, la Wifi ne peut fonctionner si il n'est pas renseigné.

### Démarrer le Raspberry
Installer la carte dans le Raspberry, brancher le câble Ethernet et l'alimentation.

### Se connecter

Retrouver l'adresse ip de votre Raspberry avec le programme [Advanced IP Scanner](https://www.advanced-ip-scanner.com/download/Advanced_IP_Scanner_2.5.3850.exe).

![][Advanced IP Scanner]

Liste des [appareils](appareils).

Utiliser le programme **PuTTY** pour se connecter en SSH :\
Nom d'utilisateur : `pi`\
Mot de passe : `raspberry`

![][PuTTY]

Pour faire fonctionner le pavé numérique, aller dans le menu Terminal – Features puis cocher _Disable application keypad mode_

Vous pouvez aussi enregistrer le nom d'utilisateur dans Connection - Data.

Enregistrer la session (save sur le profil Default Setings) pour la retrouver ces réglages à la prochaine connexion.

## Protéger l'accès

Voir le paragraphe [durcir la sécurité](../security/hardened).

## Effectuer une mise à jour du système.

> Sans être connecté à l'utilisateur root, il est nécessaire d'utiliser la précommande _sudo_ (_substitute user do ..._) à chaque ligne.
{.info}

```shell-session
apt update
apt list --upgradable
apt upgrade -y
```
Il est possible de chainer plusieurs commande sur une seule ligne, en utilisant l'opérateur _&&_

```shell-session
apt update && apt upgrade -y
```
_`apt update`_ permet de mettre à jour les catalogues contenant la liste des différentes versions des paquets disponibles.

_`apt list --upgradable`_ permet de lister les paquets qui doivent être mise à jour.

_`apt upgrade`_ permet d'appliquer les mises à jour trouvées, le commutateur -y permet d'accepter automatiquement toutes les demandes.

## Configurations supplémentaires

### Régler le fuseau horaire sur Paris.

```shell-session
timedatectl set-timezone Europe/Paris
```

### Activer la paramètres linguistiques pour la France


Générer les fichiers systèmes pour la nouvelle langue

```shell-session
localectl set-locale LANG=fr_FR.UTF-8
locale-gen
```

Vérifier que la ligne `fr_FR.UTF-8 UTF-8` est bien décommenter dans le fichier _`/etc/locale.gen`_


Redémarrer et contrôller ensuite les paramètres

```shell-session
locale
```

Si besoin revoir les [commandes](../linux) d'un système Linux.

[Advanced IP Scanner]: /img/Advanced_IP_Scanner.png "Advanced IP Scanner"
[PuTTY]: /img/PuTTY.png "PuTTY"


headless
: Mode de fonctionnement _sans tête_, utilisation d'un appareil sans écran, ni clavier, ni souris branchés directement à lui.

ssh
: Secure Shell est un protocole de communication sécurisé. Il a été conçu pour remplacer les différents protocoles non chiffrés tel que rlogin et telnet. Le chiffrement de tous les paquets de communication rend ce protocole très sûr.
