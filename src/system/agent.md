Utiliser Git Bash pour exécuter les commandes

Démarrer l'agent ssh. En utilisant la précommande eval, l'agent s'executera en arrière plan. Notez l'identifiant du process pour pouvoir l'arrêter plus tard.

```
eval "$(ssh-agent -s)"
$ Agent pid 9999
```

Ajouter la clé privée `id_rsa` dans l'agent SSH. Entrer le mot de passe de la clé à l'aide de la fonction Autotype de KeePass.

```shell-session
ssh-add ~/.ssh/id_rsa
```
