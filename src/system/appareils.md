---
layout: layouts/page.njk
title: Appareils
---

Nom|IP|Ethernet
---|---|---
iutsd-raspberry01.iutsd.site.univ-lorraine.fr | 100.75.12.65 | `B8:27:EB:9D:41:49`
iutsd-raspberry02.iutsd.site.univ-lorraine.fr | 100.75.12.66 | `B8:27:EB:5A:90:D0`
iutsd-raspberry03.iutsd.site.univ-lorraine.fr | 100.75.12.67 | `B8:27:EB:5E:95:5D`
iutsd-raspberry04.iutsd.site.univ-lorraine.fr | 100.75.12.68 | `B8:27:EB:18:A9:BF`
iutsd-raspberry06.iutsd.site.univ-lorraine.fr | 100.75.12.70 | `B8:27:EB:63:90:63`
iutsd-raspberry07.iutsd.site.univ-lorraine.fr | 100.75.12.71 | `B8:27:EB:1A:20:78`
iutsd-raspberry09.iutsd.site.univ-lorraine.fr | 100.75.12.72 | `B8:27:EB:21:0D:4E`
iutsd-raspberry13.iutsd.site.univ-lorraine.fr | 100.75.12.86 | `B8:27:EB:02:F8:6C`
iutsd-raspberry31.iutsd.site.univ-lorraine.fr | 100.75.12.104 | `B8:27:EB:7D:46:42`

30	100.64.98.61	100.64.98.61	Raspberry Pi Foundation	B8:27:EB:49:DC:C8
14	100.64.98.45	100.64.98.45	Raspberry Pi Foundation	B8:27:EB:28:61:DA
On	100.64.98.52	100.64.98.52	Raspberry Pi Foundation	B8:27:EB:C8:D4:B8
On	100.64.98.53	100.64.98.53	Raspberry Pi Foundation	B8:27:EB:0B:48:08

## GEII

Nom|IP|Ethernet|Etudiant
---|---|---|---
#1  | 100.64.98.229 | B8:27:EB:19:7C:FB | LOUIS Xavier
#2  | 100.64.98.227 | B8:27:EB:CE:B4:0B | -
#3  | 100.64.98.228 | B8:27:EB:47:9F:C8 | JACOB-AMET Léo
#4  | 100.64.98.226 | B8:27:EB:D9:4D:A9 | -
#5  | 100.64.98.190 | B8:27:EB:1E:D7:D7 | ?
#6  | 100.64.98.189 | B8:27:EB:72:34:70 | ROUZIER Théo
#3b | 100.64.98.178 | B8:27:EB:94:D4:20 | MILLER Gauthier
#8  | 100.64.98.188 | B8:27:EB:1F:88:35 | BLANCK Yann
#9  | 100.64.98.187 | B8:27:EB:1F:33:8A | MVOU Indrich
#10 | 100.64.98.186 | B8:27:EB:50:ED:2D | GEORGES Louis
#11 | 100.64.98.185 | B8:27:EB:55:85:0C | MAVETSI Marlyne
#12 | 100.64.98.184 | B8:27:EB:B5:17:B3 |
