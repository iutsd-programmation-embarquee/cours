---
layout: layouts/page.njk
title: Circuits
---

## Kit GPIO
Kit GPIO pour Raspberry Pi PI018

https://www.gotronic.fr/art-kit-gpio-pour-raspberry-pi-pi018-25862.htm

## Lecteur RFID

### RC522 13,56 MHz

https://www.gotronic.fr/art-module-rfid-13-56-mhz-tag-rc522-25651.htm
https://www.gotronic.fr/pj2-sbc-rfid-rc522-fr-1439.pdf
