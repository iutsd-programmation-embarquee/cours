# LINQ

Language Integrated Query (Requête intégrée au langage, aussi connu sous le nom de LINQ) est un composant du framework .NET de Microsoft qui ajoute des capacités d'interrogation sur des données aux langages .NET en utilisant une syntaxe proche de celle de SQL.

```csharp
IQueryable<Customer> custQuery =
    from cust in db.Customers
    where cust.City == "London"
    select cust;
```

Il existe 2 syntaxes pour effectuer les actions LINQ

## Mode procédural (method base)

```csharp
IEnumerable<int> numQuery2 = numbers.Where(num => num % 2 == 0).OrderBy(n => n);
```

## Mode Requête (query expression)

```csharp
IEnumerable<int> numQuery1 =
    from num in numbers
    where num % 2 == 0
    orderby num
    select num;
```
