---
layout: layouts/page.njk
title: Entity Framework
---

Entity Framework est le mapping objet-relationnel (ORM) utilisé par la technologie Microsoft .NET. Depuis la version 6, le projet est un paquet extérieur, il faut le rajouter à la solution.

### Créer un projet pour une application Console
```shell-session
$ dotnet new console
```

### Installer le paquet Entity Framework Design et ses outils.

```shell-session
$ dotnet tool install --global dotnet-ef
$ dotnet add package Microsoft.EntityFrameworkCore.Design
```

Installation de la commande `dotnet ef` et du paquet de conception. Permet de créer la structure de la base, de gérer les migrations, et les mises à jour.

En l'installant de manière globale, il n'est pas nécessaire de l'installer pour les projets suivants.

### Ajouter le fournisseur Entity Framework pour SQLite

SQLite est une bibliothèque écrite en langage C qui propose un moteur de base de données relationnelle accessible par le langage SQL.
Contrairement aux serveurs de bases de données traditionnels, comme PostgreSQL, MySQL SQLServer ou Oracle, sa particularité est de ne pas fonctionner le schéma habituel client-serveur mais d'être directement intégrée aux programmes. L'intégralité de la base de données (déclarations, tables, index et données) est stockée dans un fichier indépendant du système d'exploitation.

```shell-session
 $ dotnet add package Microsoft.EntityFrameworkCore.Sqlite
```

Il existe plusieurs fournisseurs de base de données pour Microsoft SQL Server, Oracle, MySQL, Postgresql, Azure Cosmos, Firebird, IBM DB2, etc.
 
### Créer les modèles de données

Créer un dossier Models

Ajouter 2 classes pour gérer les comptes bancaires et les titulaires.

```csharp
using System.ComponentModel.DataAnnotations;
using System;

namespace AppWebApi.Models
{
  public class Compte
  {
    [Key]
    public string Numéro { get; set; }
    public decimal Solde { get; private set; }
  }

  public class Titulaire
  {
    [Key]
    public int Id { get; set; }
    public string Nom { get; set; }
    public DateTime Naissance { get; set; }
  }
}
```

Les annotations comme le mot clé `[Key]` permettent d'ajouter des propriétés aux champs.


### Créer les relations entre les comptes et les titulaires.

Un titulaire peut posséder plusieurs comptes, c'est une relation de type one-to-many.

Vous remarquerez que les deux propriétés de navigation (Titulaire et Compte) sont virtuelles (virtual). Cela active la fonctionnalité de chargement différé d'Entity Framework.

Le chargement différé signifie que le contenu de ces propriétés est chargé automatiquement à partir de la base de données seulement lorsque vous tentez d’y accéder.

Il est possible d'ajouter l'identifiant (TitulaireId) en plus de la propriété elle même

```csharp
public int TitulaireId { get; set; }
public virtual Titulaire Titulaire { get; set; }

public virtual List<Compte> Comptes { get; } = new List<Compte>()
```

La liste en elle-même est en lecture seule, et est crée dés l'initialisation du Titulaire. Son contenu lui est modifiable.

### Créer la classe de contexte qui va gérer l'accès au modèle

```csharp
using AppWebApi.Models;
using Microsoft.EntityFrameworkCore;

public class BanqueContext : DbContext
{
  public DbSet<Titulaire> Titulaires { get; set; }
  public DbSet<Compte> Comptes { get; set; }

  protected override void OnConfiguring(DbContextOptionsBuilder options)
    => options.UseSqlite("Data Source=banque.db");
}
```

On peut spécifier les relations de manière plus explicite mais cela ne semble pas nécessaire dans notre cas.

```csharp
protected override void OnModelCreating(ModelBuilder modelBuilder)
{
  modelBuilder.Entity<Titulaire>().HasMany(s => s.Comptes).WithOne(s => s.Titulaire);
}
```

### Générer le script de création de la base de données et des tables

```shell-session
$ dotnet ef migrations add InitialCreate
```

### Créer la base de données

```shell-session
$ dotnet ef database update
```

## Utiliser les données

```csharp
using (var db = new BanqueContext())
{

}
```

Ajouter des données

```csharp
db.Add(new Titulaire { Nom = "Albert" });
db.SaveChanges();
```

Supprimer

```csharp
db.Remove(titulaire);
db.SaveChanges();
```

Lire / Récupérer

```csharp
var t = db.Titulaires.OrderBy(b => b.Nom).First();
```

La bibliothèque Linq est nécessaire pour effectuer des interrogations sur les collections d'objets.

```csharp
using System.Linq;
```

Pour accéder aux objets liés (comme les comptes) il est nécessaire d'inclure ces objets dans la requête

```csharp
var t = db.Titulaires.Include(t => t.Comptes).OrderBy(b => b.Nom).First();

Console.Write(t.Nom);
foreach (var c in t.Comptes)
{
  Console.WriteLine(c.Numéro);
}
```

Modifier

```csharp
t.Nom = "Jacques";
t.Comptes.Add( new Compte { Numéro = "A1234" });
```

Postgresql

Utiliser PostgreSQL comme support d'Entity Framework

PostgreSQL est un système de gestion de base de données relationnelle et objet. C'est un outil libre apprécié pour sa rapidité et sa fiabilité.

```shell-session
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL 
```

```csharp
public class BanqueContext : DbContext
{
    public DbSet<Titulaire> Titulaires { get; set; }
    public DbSet<Compte> Comptes { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseNpgsql("Host=my_host;Database=my_db;Username=my_user;Password=my_pw");
}
```