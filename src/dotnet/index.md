---
layout: layouts/page.njk
title: .NET Core
---

Récupérer le lien d'installation pour Linux ARM32 sur le dépôt officiel de [.NET Core](https://github.com/dotnet/core).

Lien .NET Core released builds puis Latest Updates .NET Core 5.0.1 et enfin récupérer le lien pour les binaires SDK Linux ARM32.

```shell-session
wget https://download.visualstudio.microsoft.com/download/pr/ad..c6/80f9...a660f/dotnet-sdk-6.0.201-linux-arm.tar.gz

mkdir -p /usr/share/dotnet
tar xfz dotnet-sdk-6.0.201-linux-arm.tar.gz -C /usr/share/dotnet/
ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet
```

Vérifier les versions du SDK installées

```shell-session
dotnet --info
```

https://docs.microsoft.com/en-us/dotnet/fundamentals/

[webapi](webapi)
