---
layout: layouts/page.njk
title: Web API
---

### Créer un projet de type webapi

Créer un dossier, ouvrir le dossier avec Visual Studio Code, et dans le terminal entrer la commande

```shell-session
dotnet new webapi
dotnet new gitignore
dotnet run
```

Un projet fonctionnel est tout de suite créé

Tester le projet en ouvrant un navigateur https://localhost:5001/WeatherForecast

Cet exemple utilise des données statiques directement dans le code
1. Ajouter des modèles pour gérer les comptes bancaires
2. Créer une classe de contexte de base de données pour EntityFramework

```csharp
using Microsoft.EntityFrameworkCore;

namespace WebApp.Models
{
  public class BanqueContext : DbContext
  {
    public BanqueContext(DbContextOptions<TodoContext> options)
        : base(options)
    {
    }

    public DbSet<Titulaire> Titulaires { get; set; }
  }
}
```

Inscrire le contexte de base de données auprès du conteneur d'injection de dépendances 

```csharp
//Procédure ConfigureServices de la classe Startup

services.AddDbContext<TodoContext>(opt =>
               opt.Usexxxx ("Banque")); 
```

Adapter le support de la base de données

`UseInMemoryDatabase` permet de créer une base de données sans support physique, uniquement en mémoire. Lorsque le programme est quitté ou redémarré toutes les données sont perdues

`UseSqlite` : support pour une base de données SQLite

`UseNpgsql` : support pour une base de données Postgresql

Générer automatiquement un contrôleur pour un modèle

```shell-session
dotnet add package Microsoft.EntityFrameworkCore.Sqlite
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL
dotnet tool install --global dotnet-ef
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet tool install --global dotnet-aspnet-codegenerator
```

```shell-session
dotnet aspnet-codegenerator controller -name TitulaireController -async -api -m Titulaire -dc BanqueContext -outDir Controllers
```

Attention : l'outil de génération fonctionne avec le support pour SQL Server même si celui-ci n'est pas utilisé par la base de données finale

```shell-session
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
```

Le code généré :

Marque la classe avec l’attribut `[ApiController]`. Cet attribut indique que le contrôleur répond aux requêtes de l’API web. 

Utilise l’injection de dépendances pour injecter le contexte de base de données (BanqueContext) dans le contrôleur. Le contexte de base de données est utilisé dans chacune des méthodes CRUD du contrôleur.

Examiner la méthode de création de Titulaire

Remplacez l’instruction return dans PostTitulaire pour utiliser l’opérateur nameof

Le code généré est une méthode HTTP Post, comme indiqué par l’attribut entre crochet [] . La méthode prend la valeur du titulaire à créer dans le corps de la requête HTTP.

La méthode CreatedAtAction :

-	Retourne un code d’état HTTP 201 en cas de réussite. HTTP 201 est la réponse standard d’une méthode HTTP POST qui crée une ressource sur le serveur.
-	Ajoute un en-tête Location à la réponse. L’en-tête Location spécifie l’URI de l’élément d’action qui vient d’être créé. 
-	Fait référence à l'action GetTitulaire pour créer l’URI Location de l’en-tête. Le mot clé C# `nameof` est utilisé pour éviter de coder en dur le nom de l’action dans l’appel CreatedAtAction.

Tester le service avec Rest Client

```
GET /api/Titulaire
```

Création

```
POST /api/Titulaire
Content-Type	application/json

{ "Nom": "Albert" }
```

Modification

```
PUT /api/Titulaire
Content-Type	application/json

{ "Id":1, "Nom": "Robert" }
```

Pour afficher les objets liés il faut les inclure
Par exemple pour afficher les comptes d'un titulaire identifié par son id

```csharp
var titulaire = await _context.Titulaires.Include(t => t.Comptes).FirstOrDefaultAsync(i => i.Id == id);
```

> Attention !
> L'utilisation de propriétés liées peuvent conduire à des références circulaires et des inclusions infinies entre les objets.

Il existe 2 méthodes pour prévenir cet effet

1. Ignorer dans la sérialisation JSON les propriétés permettant un retour vers l'objet parent, en annotant le champ en question

```csharp
using System.ComponentModel.DataAnnotations;

[JsonIgnore]
public virtual Titulaire Titulaire { get; set; }
```
2. En utilisant la librairie NewtonSoftJson qui est plus finement paramétrable que la sérialisation de base du Framework .Net Core

```shell-session
dotnet add package Microsoft.AspNetCore.Mvc.NewtonsoftJson;
```

Puis configurer dans les services (ConfigureServices) la propriété ignore sur les boucles de référence

```csharp
services.AddControllers().AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
```
 
## Routage et chemins d’URL

L’attribut `[HttpGet]` désigne une méthode qui répond à une requête HTTP GET. Le chemin d’URL pour chaque méthode est construit comme suit :

Partez de la chaîne de modèle dans l’attribut Route du contrôleur :

```csharp
[Route("api/[controller]")] 
[ApiController]
public class ComptesController : ControllerBase
{
```

Remplacez `[controller]` par le nom du contrôleur qui, par convention, est le nom de la classe du contrôleur sans le suffixe « Controller ». Pour cet exemple, le nom de la classe du contrôleur étant TodoItemsController, le nom du contrôleur est « TodoItems ». Le routage d’ASP.NET Core ne respecte pas la casse.

Si l’attribut [HttpGet] a un modèle de route (par exemple, [HttpGet("products")]), ajoutez-le au chemin. Cet exemple n’utilise pas de modèle. Pour plus d’informations, consultez Routage par attributs avec des attributs Http[Verbe].

Dans la méthode GetTodoItem suivante, `{id}` est une variable d’espace réservé pour l’identificateur unique de la tâche. Lorsque `GetTodoItem` est appelé, la valeur de `id` dans l’URL est fournie à la méthode dans son paramètre `id`.

```csharp
// GET: api/TodoItems/5
[HttpGet("{id}")] 
public async Task<ActionResult<TodoItem>> GetTodoItem(long id)
{
    var todoItem = await _context.TodoItems.FindAsync(id);

    if (todoItem == null)
    {
        return NotFound();
    }

    return todoItem;
}
```

https://docs.microsoft.com/fr-fr/aspnet/core/tutorials/first-web-api?view=aspnetcore-3.1&tabs=visual-studio-code

