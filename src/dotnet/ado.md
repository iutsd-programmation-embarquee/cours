---
layout: layouts/page.njk
title: ADO
---

## ADO.Net Data Provider

ADO.NET est un ensemble de classes qui exposent les services d'accès aux données pour les programmeurs .NET Framework. ADO.NET propose un large ensemble de composants pour la création d'applications distribuées avec partage de données. Partie intégrante du .NET Framework, il permet d'accéder à une multitude de bases des données relationnelles, XML et d'application. ADO.NET répond à divers besoins en matière de développement, en permettant notamment de créer des clients de bases de données frontaux et des objets métier de couche intermédiaire utilisés par des applications, outils, langages ou navigateurs Internet.

Ajouter à votre projet C# le package `Npgsql`

```shell-session
$ dotnet add package Npgsql
```

```csharp
var connString = "Host=localhost;Username=postgres;Database=geii";

using var conn = new NpgsqlConnection(connString);
conn.Open();

// Insert some data
using (var cmd = new NpgsqlCommand("INSERT INTO public.production (\"Document\") VALUES (@doc)", conn))
{
   cmd.Parameters.AddWithValue("doc", "A1234567");
   cmd.ExecuteNonQuery();
}

// Retrieve all rows
using (var cmd = new NpgsqlCommand("SELECT \"Document\" FROM public.production", conn))
using (var reader = cmd.ExecuteReader())
    while (reader.Read())
      Console.WriteLine(reader.GetString(0));
```
