---
layout: layouts/page.njk
title: Cinéma
---

Concevoir une base de données pour gérer une application en relation avec le cinéma.

Lister les **films**, leur _titre_, pour les films étrangers leur _titre en langue originale_, la _durée_ en minutes, l'_année de production_ et la _date de sortie en salle_.

Lister les **acteurs et l'équipe de réalisation**. Ce sont des _personnes_ qui ont un _rôle_ dans un film et un _alias_ correspondant à leur rôle. Les rôles correspondent aux métiers : acteur, réalisateur, scénariste, etc.

Lister les **personnes**, leur _prénom_, leur _nom_, leur _date de naissance_, éventuellement leur _date de décès_, leurs _nationalités_ et leur _nom d'artiste_.

Lister les **cinémas**, leur _nom_, leur _identifiant national_, et d'une _adresse_. Une **adresse** est constituée d'une _rue_, d'un _code postal_, d'une _ville_ et de _coordonnées géographiques_.

Les cinémas sont constitués de salles. Les **salles** ont un _nom_ qui peut être un numéro et une _capacité en fauteuils_.

Lister les **séances de projection**. Les séances concernent un _film_, projeté dans une _salle_ d'un cinéma à un _horaire_ donné.

Donner la possibilité d'acheter des **billets** pour une _séance_. Le ticket a un prix.

```mermaid
classDiagram

Person "1" <-- "*" CastAndCrew
Movie "1" o--> "*" CastAndCrew
Movie "1" o--> "*" Showtime
Theater "1" o--> "*" Screen
Screen "1" o--> "*" Showtime
Ticket "1" <--o "*" Showtime
Theater "1" -- "1" Address

class Person {
  uuid id
  string lastname
  string firstname
  date dob
  date? dod
  character[] nationalities
  string? artistname
  timestamp created_at
  timestamp? updated_at
}

class CastAndCrew{
  int id
  uuid movie
  uuid person
  string role
  string? alias
}

class Movie {
  uuid id
  string title
  string title_original
  int year
  date release
  int runtime
}

class Theater {
  int id
  string name
  int national_id
  address address
}

class Address {
  string street
  string zipcode
  string city
  geometry location
}

class Screen{
  int id
  int theater
  string name
  int seats
}

class Showtime{
  int id
  uuid movie
  id screen
  datetime showtime
}

class Ticket{
  String Title
}
```

```sql
-- Table: cinema.movie

DROP TABLE movie;
CREATE TABLE IF NOT EXISTS movie
(
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  title character varying(80) NOT NULL,
  title_original character varying(80),
  year integer,
  release date,
  runtime integer,
  PRIMARY KEY (id)
)
```
