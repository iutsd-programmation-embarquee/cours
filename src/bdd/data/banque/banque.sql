CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';

CREATE FUNCTION trigger_set_timestamp()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$BODY$;

ALTER FUNCTION public.trigger_set_timestamp() OWNER TO iutsd;

SET default_tablespace = '';

CREATE TABLE banque.account (
);

ALTER TABLE banque.account OWNER TO iutsd;

CREATE TABLE banque.customer (
);

ALTER TABLE banque.customer OWNER TO iutsd;

CREATE TABLE banque.transaction (
);

ALTER TABLE banque.transaction OWNER TO iutsd;
