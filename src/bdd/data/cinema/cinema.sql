CREATE TYPE address AS (
	street character varying(35),
	zipcode character varying(8),
	city character varying(35),
	location public.geometry
);

CREATE FUNCTION trigger_set_timestamp()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$BODY$;

ALTER TYPE address OWNER TO cinema;

CREATE FUNCTION moviecastandcrew(movieid uuid) RETURNS TABLE(name character varying, role character varying, alias character varying, age integer)
    LANGUAGE sql
    AS $$
SELECT
   CASE
            WHEN p.artistname IS NOT NULL THEN p.artistname::text
            ELSE (p.firstname::text || ' '::text) || p.lastname::text
        END AS name,
    c.role,
    c.alias,
	date_part('year'::text, age(make_date(m.year,7,1), p.dob::timestamp with time zone
							  ))::integer as age
   FROM ciaomovies.castandcrew c
     JOIN person p ON c.person = p.id
     JOIN ciaomovies.movie m ON c.movie = m.id
  WHERE m.id = movieid
$$;


ALTER FUNCTION moviecastandcrew(movieid uuid) OWNER TO cinema;

CREATE TABLE castandcrew (
    id integer NOT NULL,
    movie uuid NOT NULL,
    person uuid NOT NULL,
    role character varying(25),
    alias character varying(40)
);


ALTER TABLE castandcrew OWNER TO cinema;

--
-- TOC entry 224 (class 1259 OID 18059)
-- Name: castandcrew_id_seq; Type: SEQUENCE; Schema: cinema; Owner: cinema
--

CREATE SEQUENCE castandcrew_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE castandcrew_id_seq OWNER TO cinema;

ALTER SEQUENCE castandcrew_id_seq OWNED BY castandcrew.id;

CREATE TABLE movie (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    title character varying(80) NOT NULL,
    title_original character varying(80),
    year integer,
    release date,
    runtime integer
);


ALTER TABLE movie OWNER TO cinema;

--
-- TOC entry 226 (class 1259 OID 18065)
-- Name: movie_without_castandcrew; Type: VIEW; Schema: cinema; Owner: cinema
--

CREATE VIEW movie_without_castandcrew AS
 SELECT m.id,
    m.title
   FROM (movie m
     LEFT JOIN castandcrew c ON ((m.id = c.movie)))
  WHERE (c.id IS NULL);


ALTER TABLE movie_without_castandcrew OWNER TO cinema;

--
-- TOC entry 227 (class 1259 OID 18069)
-- Name: movie_without_director; Type: VIEW; Schema: cinema; Owner: cinema
--

CREATE VIEW movie_without_director AS
 SELECT m.id,
    m.title
   FROM (movie m
     LEFT JOIN castandcrew c ON (((m.id = c.movie) AND ((c.role)::text = 'director'::text))))
  WHERE (c.id IS NULL);


ALTER TABLE movie_without_director OWNER TO cinema;

--
-- TOC entry 228 (class 1259 OID 18073)
-- Name: person; Type: TABLE; Schema: public; Owner: cinema
--
CREATE TABLE public.person (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    lastname character varying(30) NOT NULL,
    firstname character varying(30),
    dob date NOT NULL,
    dod date,
    nationalities character(2)[],
    artistname character varying(30),
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.person OWNER TO cinema;
COMMENT ON COLUMN public.person.dob IS 'date of birth';

COMMENT ON COLUMN public.person.dod IS 'date of death';
CREATE VIEW person_without_movie AS
 SELECT p.id,
    p.firstname,
    p.lastname
   FROM (person p
     LEFT JOIN castandcrew c ON ((p.id = c.person)))
  WHERE (c.id IS NULL);


ALTER TABLE person_without_movie OWNER TO cinema;

CREATE TABLE screen (
    id integer NOT NULL,
    theater uuid NOT NULL,
    name character varying,
    seats integer
);

ALTER TABLE screen OWNER TO cinema;

--
-- TOC entry 232 (class 1259 OID 18099)
-- Name: screen_id_seq; Type: SEQUENCE; Schema: cinema; Owner: cinema
--

ALTER TABLE screen ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME screen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 233 (class 1259 OID 18101)
-- Name: showtime; Type: TABLE; Schema: cinema; Owner: cinema
--

CREATE TABLE showtime (
    id integer NOT NULL,
    movie uuid NOT NULL,
    screen integer NOT NULL,
    showtime timestamp with time zone NOT NULL
);


ALTER TABLE showtime OWNER TO cinema;

--
-- TOC entry 234 (class 1259 OID 18104)
-- Name: showtime_id_seq; Type: SEQUENCE; Schema: cinema; Owner: cinema
--

CREATE SEQUENCE showtime_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE showtime_id_seq OWNER TO cinema;

--
-- TOC entry 4845 (class 0 OID 0)
-- Dependencies: 234
-- Name: showtime_id_seq; Type: SEQUENCE OWNED BY; Schema: cinema; Owner: cinema
--

ALTER SEQUENCE showtime_id_seq OWNED BY showtime.id;


--
-- TOC entry 235 (class 1259 OID 18106)
-- Name: theater; Type: TABLE; Schema: cinema; Owner: cinema
--

CREATE TABLE theater (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying(35),
    national_id character varying(10),
    address address
);


ALTER TABLE theater OWNER TO cinema;

--
-- TOC entry 236 (class 1259 OID 18113)
-- Name: showtime_theater; Type: VIEW; Schema: cinema; Owner: cinema
--

CREATE VIEW showtime_theater AS
 SELECT m.title,
    m.runtime,
    t.name,
    s.showtime
   FROM (((showtime s
     JOIN screen c ON ((s.screen = c.id)))
     JOIN theater t ON ((c.theater = t.id)))
     JOIN movie m ON ((s.movie = m.id)));


ALTER TABLE showtime_theater OWNER TO cinema;

--
-- TOC entry 237 (class 1259 OID 18118)
-- Name: ticket; Type: TABLE; Schema: cinema; Owner: cinema
--

CREATE TABLE ticket (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    showtime integer,
    price numeric(4,2),
    created_at timestamp without time zone DEFAULT now(),
    serial integer NOT NULL,
    signature character varying(80)
);


ALTER TABLE ticket OWNER TO cinema;

--
-- TOC entry 238 (class 1259 OID 18123)
-- Name: ticket_serial_seq; Type: SEQUENCE; Schema: cinema; Owner: cinema
--

CREATE SEQUENCE ticket_serial_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ticket_serial_seq OWNER TO cinema;

--
-- TOC entry 4846 (class 0 OID 0)
-- Dependencies: 238
-- Name: ticket_serial_seq; Type: SEQUENCE OWNED BY; Schema: cinema; Owner: cinema
--

ALTER SEQUENCE ticket_serial_seq OWNED BY ticket.serial;


--
-- TOC entry 239 (class 1259 OID 18140)
-- Name: aeroports; Type: TABLE; Schema: flymap; Owner: cinema
--ALTER TABLE ONLY person
    ADD CONSTRAINT cinema_person_pk PRIMARY KEY (id);


--
-- TOC entry 277 (class 1259 OID 18604)
-- Name: actor; Type: MATERIALIZED VIEW; Schema: cinema; Owner: cinema
--

CREATE MATERIALIZED VIEW actor AS
 SELECT p.id,
        CASE
            WHEN (p.artistname IS NOT NULL) THEN (p.artistname)::text
            ELSE (((p.firstname)::text || ' '::text) || (p.lastname)::text)
        END AS name,
    p.dob,
        CASE
            WHEN (p.dod IS NULL) THEN date_part('year'::text, age((p.dob)::timestamp with time zone))
            ELSE NULL::double precision
        END AS age,
    p.nationalities,
    count(c.id) AS movie_count
   FROM (castandcrew c
     JOIN person p ON ((c.person = p.id)))
  WHERE ((c.role)::text = 'actor'::text)
  GROUP BY p.id
  WITH NO DATA;


ALTER TABLE actor OWNER TO cinema;

--
-- TOC entry 4575 (class 2606 OID 18613)
-- Name: castandcrew castandcrew_pk; Type: CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY castandcrew
    ADD CONSTRAINT castandcrew_pk PRIMARY KEY (id);


--
-- TOC entry 4578 (class 2606 OID 18615)
-- Name: movie movie_pkey; Type: CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY movie
    ADD CONSTRAINT movie_pkey PRIMARY KEY (id);


--
-- TOC entry 4592 (class 2606 OID 18617)
-- Name: theater pk_theater; Type: CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY theater
    ADD CONSTRAINT pk_theater PRIMARY KEY (id);


--
-- TOC entry 4595 (class 2606 OID 18619)
-- Name: ticket pk_ticket; Type: CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT pk_ticket PRIMARY KEY (id);


--
-- TOC entry 4586 (class 2606 OID 18621)
-- Name: screen screen_pkey; Type: CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY screen
    ADD CONSTRAINT screen_pkey PRIMARY KEY (id);


--
-- TOC entry 4590 (class 2606 OID 18623)
-- Name: showtime showtime_pkey; Type: CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY showtime
    ADD CONSTRAINT showtime_pkey PRIMARY KEY (id);


--
-- TOC entry 4597 (class 2606 OID 18631)
-- Name: aeroports aeroports_pkey; Type: CONSTRAINT; Schema: flymap; Owner: cinema
--
ALTER TABLE ONLY castandcrew
    ADD CONSTRAINT castandcrew_movie_fk FOREIGN KEY (movie) REFERENCES movie(id) NOT VALID;


--
-- TOC entry 4670 (class 2606 OID 18726)
-- Name: castandcrew castandcrew_person_fk; Type: FK CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY castandcrew
    ADD CONSTRAINT castandcrew_person_fk FOREIGN KEY (person) REFERENCES person(id) NOT VALID;


--
-- TOC entry 4671 (class 2606 OID 18731)
-- Name: screen screen_theater_fk; Type: FK CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY screen
    ADD CONSTRAINT screen_theater_fk FOREIGN KEY (theater) REFERENCES theater(id) NOT VALID;


--
-- TOC entry 4672 (class 2606 OID 18736)
-- Name: showtime showtime_movie_fk; Type: FK CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY showtime
    ADD CONSTRAINT showtime_movie_fk FOREIGN KEY (movie) REFERENCES movie(id) NOT VALID;


--
-- TOC entry 4673 (class 2606 OID 18741)
-- Name: showtime showtime_screen_fk; Type: FK CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY showtime
    ADD CONSTRAINT showtime_screen_fk FOREIGN KEY (screen) REFERENCES screen(id) NOT VALID;


--
-- TOC entry 4674 (class 2606 OID 18746)
-- Name: ticket ticket_showtime_fk; Type: FK CONSTRAINT; Schema: cinema; Owner: cinema
--

ALTER TABLE ONLY ticket
    ADD CONSTRAINT ticket_showtime_fk FOREIGN KEY (showtime) REFERENCES showtime(id) NOT VALID;
