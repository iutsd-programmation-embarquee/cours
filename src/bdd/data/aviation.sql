CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;
COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';

CREATE TABLE aeroports (
    code_iata character(3) NOT NULL,
    code_icao character(4),
    nom character varying(90),
    ville character varying(30),
    altitude integer,
    localisation public.geometry,
    timezone character varying(20),
    srid integer
);


ALTER TABLE aeroports OWNER TO iutsd;

--
-- TOC entry 4847 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN aeroports.altitude; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN aeroports.altitude IS 'altitude en pieds';


--
-- TOC entry 240 (class 1259 OID 18146)
-- Name: aeroports_srid_seq; Type: SEQUENCE; Schema: flymap; Owner: iutsd
--

CREATE SEQUENCE aeroports_srid_seq
    START WITH 100000
    INCREMENT BY 1
    MINVALUE 100000
    MAXVALUE 10000000
    CACHE 1;


ALTER TABLE aeroports_srid_seq OWNER TO iutsd;

--
-- TOC entry 241 (class 1259 OID 18148)
-- Name: aircraftmodel; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE aircraftmodel (
    code_iata character(3) NOT NULL,
    modele character varying(30),
    constructeur character(2),
    wtc character(1),
    code_icao character(4),
    code_famille character(3),
    pax integer,
    speed integer,
    ceiling integer,
    range integer,
    mtow integer,
    mlw integer
);


ALTER TABLE aircraftmodel OWNER TO iutsd;

--
-- TOC entry 4848 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN aircraftmodel.wtc; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN aircraftmodel.wtc IS 'Wake Turbulence Category';


--
-- TOC entry 4849 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN aircraftmodel.pax; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN aircraftmodel.pax IS 'Passengers : nombre de passager en configuration standard 3 classes';


--
-- TOC entry 4850 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN aircraftmodel.speed; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN aircraftmodel.speed IS 'kts (noeuds)';


--
-- TOC entry 4851 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN aircraftmodel.ceiling; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN aircraftmodel.ceiling IS 'ft (pieds)';


--
-- TOC entry 4852 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN aircraftmodel.range; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN aircraftmodel.range IS 'nautical mile (milles nautiques)';


--
-- TOC entry 4853 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN aircraftmodel.mtow; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN aircraftmodel.mtow IS 'lbs Maximum Take-Off Weight';


--
-- TOC entry 4854 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN aircraftmodel.mlw; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN aircraftmodel.mlw IS 'lbs Maximum Landing Weight';


--
-- TOC entry 242 (class 1259 OID 18151)
-- Name: appareils; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE appareils (
    modele character(3) NOT NULL,
    msn integer NOT NULL,
    code_icao character(6) NOT NULL,
    type character varying(20),
    premier_vol date
);


ALTER TABLE appareils OWNER TO iutsd;

--
-- TOC entry 4855 (class 0 OID 0)
-- Dependencies: 242
-- Name: COLUMN appareils.code_icao; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN appareils.code_icao IS 'icao 24 code hexadécimal Automatic Dependent Surveillance (ADS)';


--
-- TOC entry 243 (class 1259 OID 18154)
-- Name: codeshare; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE codeshare (
    vol character varying(6) NOT NULL,
    codeshare character varying(6) NOT NULL,
    operator character(2)
);


ALTER TABLE codeshare OWNER TO iutsd;

--
-- TOC entry 244 (class 1259 OID 18157)
-- Name: constructeurs; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE constructeurs (
    code character(2) NOT NULL,
    nom character varying(30)
);


ALTER TABLE constructeurs OWNER TO iutsd;

--
-- TOC entry 245 (class 1259 OID 18160)
-- Name: routes; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE routes (
    operateur character(2) NOT NULL,
    aeroport_origine character(3) NOT NULL,
    aeroport_destination character(3) NOT NULL,
    heure_depart time without time zone,
    heure_arrivee time without time zone,
    jour_lundi bit(1),
    vol character varying(6) NOT NULL,
    jour_mardi bit(1),
    jour_mercredi bit(1),
    jour_jeudi bit(1),
    jour_vendredi bit(1),
    jour_samedi bit(1),
    jour_dimanche bit(1),
    callsign character varying(8),
    route public.geometry,
    fret numeric(4,1),
    sieges smallint
);


ALTER TABLE routes OWNER TO iutsd;

--
-- TOC entry 4856 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN routes.heure_depart; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN routes.heure_depart IS 'STD : Scheduled Time of Departure';


--
-- TOC entry 4857 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN routes.heure_arrivee; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN routes.heure_arrivee IS 'STA : Scheduled Time of Arrival';


--
-- TOC entry 4858 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN routes.fret; Type: COMMENT; Schema: flymap; Owner: iutsd
--

COMMENT ON COLUMN routes.fret IS 'en tonnes';


--
-- TOC entry 246 (class 1259 OID 18166)
-- Name: vol; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE vol (
    identification character varying(6) NOT NULL,
    appareil_immatriculation character varying(10),
    depart timestamp with time zone NOT NULL,
    arrivee timestamp with time zone,
    statut vol_statut,
    id integer NOT NULL,
    aeroport_origine character(3)
);


ALTER TABLE vol OWNER TO iutsd;

--
-- TOC entry 247 (class 1259 OID 18169)
-- Name: departs; Type: VIEW; Schema: flymap; Owner: iutsd
--

CREATE VIEW departs AS
 SELECT DISTINCT a.code_iata,
    a.nom,
    a.ville,
    public.st_y(a.localisation) AS lat,
    public.st_x(a.localisation) AS lng
   FROM ((vol v
     JOIN routes r ON (((r.vol)::text = (v.identification)::text)))
     JOIN aeroports a ON ((a.code_iata = r.aeroport_origine)));


ALTER TABLE departs OWNER TO iutsd;

--
-- TOC entry 248 (class 1259 OID 18174)
-- Name: flottes; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE flottes (
    immatriculation character varying(10) NOT NULL,
    operateur character(2) NOT NULL,
    appareil character(6),
    dates daterange
);


ALTER TABLE flottes OWNER TO iutsd;

--
-- TOC entry 249 (class 1259 OID 18180)
-- Name: operator; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE operator (
    code_iata character(2) NOT NULL,
    code_icao character(3),
    nom character varying(40),
    callsign character varying(20),
    pays public."iso-3166-a2"
);


ALTER TABLE operator OWNER TO iutsd;

--
-- TOC entry 250 (class 1259 OID 18186)
-- Name: timezone; Type: TABLE; Schema: flymap; Owner: iutsd
--

CREATE TABLE timezone (
    gid integer NOT NULL,
    zone double precision,
    sq_mi double precision,
    sqkm double precision,
    colormap double precision,
    __oid integer,
    geom public.geometry(MultiPolygon)
);


ALTER TABLE timezone OWNER TO iutsd;

--
-- TOC entry 251 (class 1259 OID 18192)
-- Name: timezone_gid_seq; Type: SEQUENCE; Schema: flymap; Owner: iutsd
--

CREATE SEQUENCE timezone_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE timezone_gid_seq OWNER TO iutsd;

--
-- TOC entry 4859 (class 0 OID 0)
-- Dependencies: 251
-- Name: timezone_gid_seq; Type: SEQUENCE OWNED BY; Schema: flymap; Owner: iutsd
--

ALTER SEQUENCE timezone_gid_seq OWNED BY timezone.gid;


--
-- TOC entry 252 (class 1259 OID 18194)
-- Name: vol_id_seq; Type: SEQUENCE; Schema: flymap; Owner: iutsd
--

CREATE SEQUENCE vol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vol_id_seq OWNER TO iutsd;

--
-- TOC entry 4860 (class 0 OID 0)
-- Dependencies: 252
-- Name: vol_id_seq; Type: SEQUENCE OWNED BY; Schema: flymap; Owner: iutsd
--

ALTER SEQUENCE vol_id_seq OWNED BY vol.id;


--
-- TOC entry 253 (class 1259 OID 18196)
-- Name: agenda; Type: TABLE; Schema: musique; Owner: iutsd
--CREATE TYPE routes_great_circle AS (
	st_transform public.geometry,
	vol character varying(6),
	operateur character(2),
	aeroport_origine character(3),
	aeroport_destination character(3)
);


ALTER TYPE routes_great_circle OWNER TO iutsd;

--
-- TOC entry 2020 (class 1247 OID 18028)
-- Name: vol_statut; Type: TYPE; Schema: flymap; Owner: iutsd
--

CREATE TYPE vol_statut AS ENUM (
    'programmé',
    'en vol',
    'annulé',
    'arrivé',
    'disparu',
    'accident'
);


ALTER TYPE vol_statut OWNER TO iutsd;

--
-- TOC entry 2023 (class 1247 OID 18043)
-- Name: adresse; Type: TYPE; Schema: public; Owner: iutsd
--
--
-- TOC entry 1557 (class 1255 OID 18051)
-- Name: aeroports_insert_projection(); Type: FUNCTION; Schema: flymap; Owner: iutsd
--

CREATE FUNCTION aeroports_insert_projection() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
   srid integer := nextval('aviation.aeroports_srid_seq');
BEGIN

INSERT INTO spatial_ref_sys (srid, auth_name, proj4text) VALUES (srid, CONCAT('azimuthal equidistant ', NEW.ville), CONCAT('+proj=aeqd +lat_0=', ST_Y(NEW.localisation), ' +lon_0=', ST_X(NEW.localisation), ' +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs') );

NEW.srid := srid;

RETURN NEW;

END
$$;


ALTER FUNCTION aeroports_insert_projection() OWNER TO iutsd;

--
-- TOC entry 1560 (class 1255 OID 18052)
-- Name: aeroports_update_projection(); Type: FUNCTION; Schema: flymap; Owner: iutsd
--

CREATE FUNCTION aeroports_update_projection() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

UPDATE spatial_ref_sys SET auth_name = CONCAT('azimuthal equidistant ', NEW.ville), proj4text = CONCAT('+proj=aeqd +lat_0=', ST_Y(NEW.localisation), ' +lon_0=', ST_X(NEW.localisation), ' +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs') WHERE srid = NEW.srid;

RETURN NEW;

END
$$;


ALTER FUNCTION aeroports_update_projection() OWNER TO iutsd;

--
-- TOC entry 1561 (class 1255 OID 18053)
-- Name: route_grand_cercle(); Type: FUNCTION; Schema: flymap; Owner: iutsd
--

CREATE FUNCTION route_grand_cercle() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  dateline geometry;
  route geometry;
  intersec geometry;
  frac float := 0;
  aeqd_srid integer;
BEGIN

RAISE INFO 'start % %', NEW.aeroport_origine, NEW.aeroport_destination;

--DELETE FROM spatial_ref_sys WHERE srid = '99999';
--INSERT INTO spatial_ref_sys (srid, auth_name, proj4text) VALUES ('99999', 'azimuthal equidistant', (SELECT CONCAT('+proj=aeqd +lat_0=', ST_Y(localisation), ' +lon_0=', ST_X(localisation), ' +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs' ) FROM aviation.aeroports WHERE code_iata = NEW.aeroport_origine));

--RAISE INFO 'set srid : done';

SELECT srid FROM aviation.aeroports WHERE code_iata = NEW.aeroport_origine INTO aeqd_srid;

dateline := ST_Transform(ST_Segmentize(ST_GeomFromText('LINESTRING(180 90, 180 -90)', 4326),5), aeqd_srid);

--RAISE INFO 'dateline : %', ST_AsText(dateline);

SELECT ST_MakeLine(ST_Transform(a.localisation, aeqd_srid), ST_Transform(b.localisation, aeqd_srid))
 FROM
   aviation.aeroports a, aviation.aeroports b
 WHERE
 (a.code_iata = NEW.aeroport_origine) AND
 (b.code_iata = NEW.aeroport_destination)
INTO route;

--RAISE INFO 'route : %', ST_AsText(route);

intersec := ST_GeometryN(ST_INTERSECTION(route, dateline), 1);

RAISE INFO 'intersec : %', ST_AsText(intersec);

IF intersec IS NULL THEN
	NEW.route := ST_Transform(ST_Segmentize(route, 50000), 4326);
ELSE
        frac := ST_LineLocatePoint(route, intersec);
        RAISE INFO 'frac : %', frac;
        IF frac > 0 AND  frac < 1 THEN
		NEW.route := ST_Transform(ST_Segmentize(ST_UNION(ST_Line_Substring(route,0, frac-0.01), ST_Line_Substring(route,frac+0.01, 1)),50000),4326);
	ELSE
		NEW.route := ST_Transform(ST_Segmentize(route, 50000), 4326);
	END IF;
END IF;

--RAISE INFO 'new.route : done';

RETURN NEW;

END
$$;


ALTER FUNCTION route_grand_cercle() OWNER TO iutsd;

--
-- TOC entry 1562 (class 1255 OID 18054)
-- Name: toto(); Type: FUNCTION; Schema: public; Owner: iutsd
--

CREATE FUNCTION public.toto() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
   tata    integer := 1;
BEGIN
  select LAG(signature,1) OVER (ORDER BY serial) previous_signature into tata
  FROM cinema.ticket
ORDER BY id DESC;
  NEW.signature = tata || NEW::text;
  RETURN NEW;
END;
$$;
